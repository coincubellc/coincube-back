# Define the application directory
import os
import base64
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# define directly in instance config 
DATABASE_CONNECT_OPTIONS = {}

SQLALCHEMY_TRACK_MODIFICATIONS = False

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection against *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
# NOTE: define again in instance config for production
CSRF_SESSION_KEY = "7q0gsvnpv52b0c5f4ay$1uro9awzwcuwa*k6j2om9urj!8qr63*jemzipm9&"

# Secret key for signing cookies
SECRET_KEY = "c1p#vhl!e4Yagb$og3C3!eSoX7o7PsDSx3VAx#e9x44!k@jodf9?o7xk06cpaggqq8mk9gvd68#6zhto&vjb0cf2"

# Configure Flask-Mail
MAIL_DEFAULT_SENDER = 'admin@coincube.io'

# Configure Social Signin Credentials
OAUTH_CREDENTIALS = {
    'facebook': {
        'id': '862174757218457',
        'secret': '8587f3c70795b8815d3fd9799f7f61f1'
    },
    'coinbase': {
        'id': 'c8dbd8306677ffec3d67526528e197db2a7fe9358e50c64b6abb6f36ab040385',
        'secret': '83cc1abe5e15a7166a0f11da5d8d5dcb5f2795bcd8248fcbbf4bc9f60ff4e626'
    },
    'twitter': {
        'id': 'kQaHiQpR6NyFXR5duBLlB9jrA',
        'secret': '2n8JmMTChn97WhGxgfWRPchQfr6SHN2lEkkCGtehDuL83Q9Grp'
    }
}

# APP config
APP_TITLE = "COINCUBE"

# Email Addresses
ADMIN_EMAIL = MAIL_DEFAULT_SENDER
OPS_EMAIL = "eric@coincube.io"
SUPPORT_EMAIL = "support@coincube.io"

# SMTP Server
MANDRILL_USERNAME = "robert@coincube.io"
MANDRILL_PASSWORD = "fYtRDHAHdM8IPPpre0FFLg" 

# COINCUBE billing
MIN_PAYMENT_BTC = .05
MIN_MAINTENANCE_BTC = .02
FLAT_FEE_MULTIPLIER = .01
MAX_GRACE_BTC = .25
GRACE_PERIOD_DAYS = 45

# Trade minimum
MIN_TRADE = 0.0005

BASE_URL = "https://coincube.io"