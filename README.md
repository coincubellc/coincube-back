<h2>coincube-back</h2>
This repo contains the Coincube Flask Web Application API.

<h3>Getting Started</h3>
In order to get up and running with this repo:

<h3>Docker Setup</h3>
You will need <a href="https://docker.com" target="_blank">Docker</a>.

<h3> Database Submodule</h3>
This repo uses one submodule which you will need to pull in:
`git submodule init` <br>
`git submodule update` <br>

Build the Docker container(s):
For local environment using MariaDB: `docker-compose build`<br>

To use AWS development database: `docker-compose -f docker-compose.dev.yml build`

Run the Docker container(s):
For local environment using MariaDB: `docker-compose up`<br>

To use AWS development database: `docker-compose -f docker-compose.dev.yml up -d`

<h3>IMPORTANT</h3>
The first time you run these containers you will then need to import the `coincube.sql` database into the empty Docker Volume MariaDB. <br>
In order to do this: <br>
`mysql -h 127.0.0.1 -u develop -P 3306 -p`<br>
Once connected to the database:<br>
`source PATH/TO/DATABASE/FILE`

<h3>API Documentation</h3>
Once the application is running, you can view Swagger documentation at:
<a href="http://0.0.0.0:443/swagger-ui">http://0.0.0.0:443/swagger-ui</a><br>
JSON is available at: <a href="http://0.0.0.0:443/swagger">http://0.0.0.0:443/swagger</a>

<h3> To run database migrations</h3>
`docker exec -it <container_id> bash`
`python manage.py db init`
`python manage.py db migrate` -> must check/edit migration script in /migrations
`python manage.py db upgrade`
`python manage.py db --help`

<h3>Vault Environment Variables</h3>
If you are working on anything which requires use of data encryption/decryption in the database such as adding a user password or API key, you will need to add the `VAULT_URL` and `VAULT_TOKEN` to the `docker-compose.yml` or `docker-compose.local-db.yml` file. <br>
Robert will share this info via Keybase.<br>

<h3>Launch the App</h3>
Once Docker is running, the api endpoints will be available at `https://0.0.0.0:443`<br>

<h6>Working on the App</h6>
Any time you save a `.py` file with changes, the Flask application will reload.<br>

<h3>Troubleshooting</h3>
Make sure that old docker networks are removed<br>
'docker-compose down' should remove them<br>
Check with 'docker network ls'<br>
If you see errors related to Disk Space, you may need to reset Docker:<br>
To do so: Docker -> Preferences -> Reset -> Remove All Data