from flask_apispec import MethodResource, marshal_with, doc
from schemas import TeamSchema
from database import Team


@marshal_with(TeamSchema(many=True))
@doc(tags=['Content'], description='Team content')
class TeamResource(MethodResource):
    def get(self):
        return Team.query.all()