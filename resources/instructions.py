from flask_apispec import MethodResource, marshal_with, doc
from schemas import InstructionSchema
from database import Instruction

@marshal_with(InstructionSchema)
class InstructionResource(MethodResource):
    @doc(tags=['Content'], description='Instructions content. Keys=("connection", "address")')
    def get(self, key):
        return Instruction.query.filter_by(key=key).first()