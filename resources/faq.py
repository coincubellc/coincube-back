from flask_apispec import MethodResource, marshal_with, doc
from schemas import FAQSchema
from database import FAQTOC


@marshal_with(FAQSchema(many=True))
@doc(tags=['Content'], description='FAQ content')
class FAQResource(MethodResource):
    def get(self):
        return FAQTOC.query.all()