import pandas as pd
from flask_restful import abort
from flask_apispec import MethodResource, marshal_with, doc
from schemas import PerformanceChartSchema
from database import BacktestResults, Focus
from .tools.cube import create_series_chart


@doc(tags=['Charts'], 
    description='Performance chart by index_name='\
    'top_five, top_ten, top_twenty, top_thirty, top_fifty, top_hundred'\
    ' index_type= mcw (market cap weighted), ew (equally weighted)')
class PerformanceChart(MethodResource):
    def get(self, index_type, index_name):
        try:
            '''Retrieve Index Backtest'''
            full_index_name = f'{index_name}_{index_type}'
            query = BacktestResults.query.filter_by(index_name=full_index_name)
            try:
                data = pd.read_sql(query.statement, query.session.bind, index_col='timestamp')
            except ValueError:
                abort(404)

            performance = create_series_chart(data.portfolio_value.copy())

            return [{ 'index_type': full_index_name, 
                    'performance': performance}]
        except:
            abort(500, message='Something went wrong')


@doc(tags=['Charts'], description='All performance charts by index_type= mcw'\
                      '(market cap weighted), ew (equally weighted)')
class PerformanceCharts(MethodResource):
    def get(self, index_type):
        try:
            '''Retrieve Index Backtests'''
            index_backtest_charts = []
            indices = Focus.query.all()
            for index in indices:
                # daily backtest data
                full_index_name = f'{index.type}_{index_type}'
                query = BacktestResults.query.filter_by(index_name=full_index_name)
                try:
                    data = pd.read_sql(query.statement, query.session.bind, index_col='timestamp')
                except ValueError:
                    abort(404)

                performance = create_series_chart(data.portfolio_value.copy())

                index_backtest_charts.append({ 'index_type': full_index_name, 
                                             'performance': performance})

            return index_backtest_charts
        except:
            abort(500, message='Something went wrong')