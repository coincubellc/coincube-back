import os
from flask_apispec import MethodResource, doc, marshal_with, use_kwargs as use_kwargs_doc
from flask_restful import abort
from webargs import fields, validate
from webargs.flaskparser import use_kwargs 
from marshmallow import missing
from sqlalchemy import or_
from flask_jwt_extended import jwt_required, get_jwt_identity
from database import (Balance, Cube, Currency, db_session, Exchange,
                      Transaction, Order, User)
from schemas import BalanceSchema, CubeSchema, OrderSchema, TransactionSchema
from .tools.cube import *
from .tools.account import reset_cube, delete_cube


# ----------------------------------------------- Wallet Resources

post_args = {
    'wallet_id': fields.Int(required=True, description='Wallet ID'),
}


class AddWallet(MethodResource):
    ext_args = {
        'name': fields.Str(required=True, description='Wallet name'),
        'wallet_type': fields.Str(required=True, 
                description='Wallet type: ("address", "manual")'),
    }
    @jwt_required
    @use_kwargs(ext_args, locations=('json', 'form'))
    @use_kwargs_doc(ext_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Add a new wallet.')
    def post(self, name, wallet_type):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Make sure wallet name hasn't already been used
        old_wallet = Cube.query.filter_by(
                            user_id=user.id, 
                            name=name
                            ).first()
        if old_wallet:
            abort(403, message='Name already used')
        if wallet_type == 'manual':
            ex = Exchange.query.filter_by(name='Manual').one()
        else:
            ex = Exchange.query.filter_by(name='External').one()
        wallet = Cube(
                user=user,
                auto_rebalance=False,
                algorithm_id=7, #Tracker default
                name=name,
                exchange_id=ex.id,
            )
        wallet.save_to_db()
        message = 'Wallet added successfully'
        return {'message': message, 'wallet_id': wallet.id}


class WalletAllocationsCurrent(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Retrieves current asset allocations for Wallet.')
    def post(self, wallet_id):
        cube = Cube.query.get(wallet_id)
        if cube:
            try:
                balances, total = get_balance_data([cube], cube.user)
                allocations = asset_allocations_from_balances(balances)
                return {'allocations_current': allocations}
            except:
                return {}
        else:
            return {}


class WalletAvailableAssets(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Retrieves exchange supported assets for Wallet.')
    def post(self, wallet_id):
        cube = Cube.query.get(wallet_id)
        if cube:
            supported_assets = cube.supported_assets
            return {'supported_assets': supported_assets}
        else:
            message = 'No wallet associated with ID {}'.format(wallet_id)
            abort(404, message=message)


class WalletBalances(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Retrieves balances for Wallet.')
    def post(self, wallet_id):
        cube = Cube.query.get(wallet_id)
        if cube:
            try:
                # Must pass in list of cubes
                balances, total, performance_fiat = get_balance_data([cube], cube.user)
                current_allocations = asset_allocations_from_balances(balances)
                return {
                    'balances': balances, 
                    'current_allocations': current_allocations,
                    'total': total,
                    'performance_fiat': performance_fiat,
                    }
            except:
                return {}
        else:
            return {}


class WalletExternalBlockchains(MethodResource):
    @jwt_required
    @doc(tags=['Wallet'], description='List of supported blockchains')
    def get(self):
        currencies = Currency.query.filter_by(external_blockchain=True).all()
        blockchains = []
        for cur in currencies:
            blockchains.append(cur.name)
        return blockchains


@marshal_with(CubeSchema())
class WalletResource(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Wallet object')
    def post(self, wallet_id):
        cube = Cube.query.get(wallet_id)
        if cube:
            return cube
        else:
            message = 'No cube associated with ID {}'.format(wallet_id)
            abort(404, message=message)


class WalletPerformanceResource(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Returns daily balances and returns for Wallet.')
    def post(self, wallet_id, timeframe):
        cube = Cube.query.get(wallet_id)
        if cube:
            try:
                dp = cube.get_performance(timeframe=timeframe)
                # Return and balance charts
                fiat_pct_returns = create_series_chart(dp.fiat_pct.copy()) or []
                btc_pct_returns = create_series_chart(dp.btc_pct.copy()) or []
                fiat_balances = create_series_chart(dp.fiat_total.copy()) or []
                btc_balances = create_series_chart(dp.btc_total.copy()) or []

                return {
                    'fiat_pct_returns': fiat_pct_returns,
                    'btc_pct_returns': btc_pct_returns,
                    'fiat_balances': fiat_balances,
                    'btc_balances': btc_balances,
                    }
            except:
                return {}
        else:
            return {}


class WalletExternalAddressResource(MethodResource):
    ext_args = {**post_args, **{
        'address': fields.Str(required=True, description='External address'),
        'blockchain': fields.Str(required=True, description='Blockchain name')
    }}
    @jwt_required
    @use_kwargs(ext_args, locations=('json', 'form'))
    @use_kwargs_doc(ext_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Add external address to Cube.')
    def post(self, wallet_id, address, blockchain):
        cube = Cube.query.get(wallet_id)
        if cube:
            # External addresses
            external_addresses = [(b.address) for b in cube.external_addresses]
            if address in external_addresses:
                message = 'Address already added'
                abort(403, message=message)
            message = add_external_address(cube, address, blockchain)
            return {'message': message}
        else:
            message = 'No wallet associated with ID {}'.format(wallet_id)
            abort(404, message=message)

    ext_args = {**post_args, **{
        'address': fields.Str(required=True, description='External Address'),
    }}
    @jwt_required
    @use_kwargs(ext_args, locations=('json', 'form'))
    @use_kwargs_doc(ext_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Remove external address from Cube.')
    def delete(self, wallet_id, address):
        cube = Cube.query.get(wallet_id)
        if cube:
            message = remove_external_address(cube, address)
            return {'message': message}
        else:
            message = 'No wallet associated with ID {}'.format(wallet_id)
            abort(404, message=message)


class WalletRefreshExternalBalances(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Retrieves balances for wallet external addresses from crypto-balances.')
    def post(self, wallet_id):
        cube = Cube.query.get(wallet_id)
        if cube:
            return get_all_external_balances(cube)
        else:
            message = 'No wallet associated with ID {}'.format(wallet_id)
            abort(404, message=message)
            

class SaveWalletSetting(MethodResource):
    ext_args = {**post_args, **{
        'name': fields.Str(required=True, description='Setting name'),
    }}
    @jwt_required
    @use_kwargs(ext_args, locations=('json', 'form'))
    @use_kwargs_doc(ext_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Update settings. "name"=("reset", "delete")')
    def post(self, wallet_id, name):
        cube = Cube.query.get(wallet_id)
        if cube:
            if name in ["reset"]:
                if reset_cube(cube.id):
                    cube.log_user_action(str(cube.id) + " reset.")
                else:
                    abort(500)
            elif name in ["delete"]:
                if delete_cube(cube.id):
                    return {'message': f'Wallet {wallet_id} deleted'}
                else:
                    abort(500)
            cube.save_to_db()
            return {'message': 'Setting successfully saved'}
        else:
            message = 'No wallet associated with ID {}'.format(wallet_id)
            abort(404, message=message)


@marshal_with(TransactionSchema(many=True))
class WalletTransactions(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Retrieves transactions for wallet.')
    def post(self, wallet_id):
        txs = Transaction.query.filter(
                    Transaction.cube_id == wallet_id,
                    Transaction.type.in_(['convert']),
                    or_(
                        Transaction._base_amount != 0,
                        Transaction._quote_amount != 0
                        )
                    ).order_by(
                        Transaction.created_at.desc()
                    ).all()
        if txs:
            return txs
        else:
            return []


class WalletValuations(MethodResource):
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Wallet'], description='Retrieves Wallet BTC and fiat valuations.')
    def post(self, wallet_id):
        cube = Cube.query.get(wallet_id)
        if cube:
            try:
                return cube.valuations()
            except:
                return []
        else:
            message = 'No wallet associated with ID {}'.format(wallet_id)
            abort(404, message=message) 
