from datetime import datetime, timedelta
import math
import pandas as pd
import numpy as np
from flask import make_response
from flask_apispec import MethodResource, doc, marshal_with, use_kwargs as use_kwargs_doc
from flask_restful import abort
from flask_jwt_extended import (jwt_required, get_jwt_identity, 
                                create_access_token, create_refresh_token)
from webargs.flaskparser import use_kwargs
from webargs import fields
from sqlalchemy import func, and_, not_
from sqlalchemy.orm import joinedload

from schemas import (InvoiceSchema, PaymentSchema)
from database import (Algorithm, Connection, Cube, db_session, Invoice, Ledger, User, UserProduct)


def authenticate_admin(email):
    user = User.query.filter_by(email=email).first()
    if user.role != 'Admin':
        abort(403)

def create_series_chart(df):
    df.index = (df.index.values.astype(float) / 1000000).astype(float)
    return [list(p) for p in df.iteritems() if not math.isnan(p[1])]

def login_user(user):
    access_token = create_access_token(identity = user.email)
    refresh_token = create_refresh_token(identity = user.email)
    resp = make_response(user.role, 200)
    resp.headers.extend({'authorization': {
        'access_token': access_token,
        'refresh_token': refresh_token
        }})
    return resp


class LoginAsUser(MethodResource):
    post_args = {
        'user_id': fields.Int(required=True, description='User ID'),
    }
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Admin'], description='Login as a selected user')
    def post(self, user_id):
        email = get_jwt_identity()
        authenticate_admin(email)
        user = User.query.get(int(user_id))
        return login_user(user)


class Users(MethodResource):
    @jwt_required
    @doc(tags=['Admin'], description='Retrieves users')
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)

        # Get users
        active = and_(
            Cube.closed_at == None,
            not_(Cube.connections.any(Connection.failed_at != None)),
            Cube.connections.any()
            )

        users = User.query.join(Cube).filter(
                        active,
                        Cube.algorithm.has(Algorithm.name.in_(['Centaur', 'Index', 'Tracker']))
                    ).options(joinedload(User.cubes)).all()
        active_users = []
        for user in users:
            active_users.append([user.email, user.id])
        return active_users



class UserStats(MethodResource):
    @jwt_required
    @doc(tags=['Admin'], description='Retrieves user statistics')
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)

        # Total user accounts
        all_users = len(User.query.all())

        # Total active users within the last 30 days
        since = datetime.utcnow() - timedelta(days=30)
        active_users = len(User.query.filter(User.last_login >= since).all())
        inactive_users = all_users - active_users

        # Total accounts with 2fa
        second_factor_users = len(User.query.filter_by(otp_complete=True).all())
        no_second_factor_users = all_users - second_factor_users

        # User acquisition line graph
        count = db_session.query(
            User.created_at, 
            func.count(User.id)
            ).group_by(
                func.date(User.created_at)).all()
        try:
            # Create dataframe with date only for rows
            df = pd.DataFrame(count, columns=('datetime', 'count'))
            df.index = df.datetime.dt.date
            del df['datetime']
            # Fill in missing dates with 0
            idx = pd.date_range(df.index[0], df.index[-1])
            df = df.reindex(idx, fill_value=0)  
            count = df['count'].astype(float)
            # Create chart
            acquisition_chart = create_series_chart(count)
        except Exception as e:
            acquisition_chart = []
        # Get free/basic/pro user numbers
        now = datetime.utcnow()
        basic_accounts = len(UserProduct.query.filter_by(
                                        product_type='basic'
                                        ).filter(
                                        UserProduct.paid_until >= now
                                        ).all())
        pro_accounts = len(UserProduct.query.filter_by(
                                        product_type='pro'
                                        ).filter(
                                        UserProduct.paid_until >= now
                                        ).all())
        free_accounts = active_users - (basic_accounts + pro_accounts)
        return {
            'users': {
                'active': active_users,
                'inactive': inactive_users,
                'all': all_users,
                'second_factor': second_factor_users,
                'no_second_factor': no_second_factor_users,
                'free': free_accounts,
                'basic': basic_accounts,
                'pro': pro_accounts,
            },
            'charts': {
                'acquisitions': acquisition_chart
            },
        }


class CubeStats(MethodResource):
    @jwt_required
    @doc(tags=['Admin'], description='Retrieves cube statistics')
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)

        # Total cubes 
        all_cubes = len(Cube.query.filter(Cube.exchange_id != 12).all())

        # Trade engine active/inactive
        active_cubes = len(Cube.query.filter(
            Cube.exchange_id != 12).filter_by(
            trading_status='live').all())
        inactive_cubes = all_cubes - active_cubes

        # Active cubes pie chart
        active_cubes_pie_chart = [
            {'name': 'Active Cubes', 'y': active_cubes},
            {'name': 'Inactive Cubes', 'y': inactive_cubes}
        ]

        # Auto rebalance
        auto_rebalance_cubes = len(Cube.query.filter(
            Cube.exchange_id != 12).filter_by(
            auto_rebalance=True).all())
        manual_rebalance_cubes = all_cubes - auto_rebalance_cubes

        # Auto rebalance pie chart
        auto_rebalance_cubes_pie_chart = [
            {'name': 'Auto Rebalance Cubes', 'y': auto_rebalance_cubes},
            {'name': 'Manual Cubes', 'y': manual_rebalance_cubes}
        ]

        # Cube balanced within last 30 days
        balance_times = {
            'Day': 1,
            'Week': 7,
            'Month': 30,
        }
        balanced_within = []
        for key, val in balance_times.items():
            since = datetime.utcnow() - timedelta(days=val)
            total = len(Cube.query.filter(
                Cube.exchange_id != 12).filter(
                Cube.balanced_at >= since).all())
            balanced_within.append({'name': key, 'y': total})

        # Cube rebalance intervals
        intervals = {
            'Hourly': 3600, 
            'Daily': 86400, 
            'Weekly': 604800,
            'Monthly': 2412900
            }
        rebalance_intervals = []
        for key, val in intervals.items():
            total = len(Cube.query.filter(
                Cube.exchange_id != 12).filter_by(
                rebalance_interval=val).all())
            rebalance_intervals.append({'name': key, 'y': total})

        # Cube algorithm breakdown
        algorithms = Algorithm.query.filter_by(active=True).all()
        cube_algorithms = []
        for algo in algorithms:
            total = len(Cube.query.filter_by(algorithm_id=algo.id).all())
            cube_algorithms.append({'name': algo.name, 'y': total})

        # Rebalancing/balanced cubes
        cubes = Cube.query.filter(
                    Cube.exchange_id != 12).filter_by(
                    trading_status='live').all()

        is_rebalancing = 0
        rebalanced = 0
        for cube in cubes:
            if cube.is_rebalancing:
                is_rebalancing += 1
            else:
                rebalanced += 1
        # Active cubes pie chart
        balanced_cubes_pie_chart = [
            {'name': 'Balancing', 'y': is_rebalancing},
            {'name': 'Balanced', 'y': rebalanced}
        ]

        return {
            'cubes': {
                'all': all_cubes,
                'active': active_cubes,
                'inactive': inactive_cubes,
                'auto': auto_rebalance_cubes,
                'manual': manual_rebalance_cubes,
            },
            'charts': {
                'balanced_within': balanced_within,
                'rebalance_intervals': rebalance_intervals,
                'algorithms': cube_algorithms, 
                'active': active_cubes_pie_chart,
                'auto_rebalance': auto_rebalance_cubes,
                'balanced': balanced_cubes_pie_chart,
            }
        }


class BillingStats(MethodResource):
    @jwt_required
    @doc(tags=['Admin'], description='Retrieves billing statistics')
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)

        # Total paid invoices
        paid_invoices = Invoice.query.filter(Invoice.paid_at != None).all()
        total_paid = 0
        for invoice in paid_invoices:
            total_paid += invoice.amount_paid

        # Invoices paid within last 30 days
        since = datetime.utcnow() - timedelta(days=30)
        paid_invoices = Invoice.query.filter(Invoice.paid_at >= since).all()
        month_paid = 0
        for invoice in paid_invoices:
            month_paid += invoice.amount_paid 

        # Payment received line graph
        count = db_session.query(
            Invoice.paid_at, 
            func.sum(Invoice.amount_paid)
            ).filter(
            Invoice.paid_at != None
            ).group_by(
                func.date(Invoice.paid_at)).all()
        # Create dataframe with date only for rows
        df = pd.DataFrame(count, columns=('datetime', 'count'))
        df.datetime = df.datetime.astype(datetime)
        df.index = df.datetime.dt.date
        del df['datetime']
        # Fill in missing dates with 0
        idx = pd.date_range(df.index[0], df.index[-1])
        df = df.reindex(idx, fill_value=0)  
        count = df['count'].astype(float)
        try:
            # Create chart
            payment_chart = create_series_chart(count)
        except Exception as e:
            payment_chart = []    

        return {
            'payment': {
                'total': total_paid,
                'month': month_paid,
            },
            'charts': {
                'payments': payment_chart,
            }
        }   


@marshal_with(InvoiceSchema(many=True))
@doc(tags=['Admin'], description='User invoices')
class AdminPaidInvoices(MethodResource):
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)
        return Invoice.query.filter(Invoice.paid_at != None).all()


@marshal_with(InvoiceSchema(many=True))
@doc(tags=['Admin'], description='User invoices')
class AdminUnpaidInvoices(MethodResource):
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)
        return Invoice.query.filter(Invoice.paid_at == None).all()


@marshal_with(PaymentSchema(many=True))
@doc(tags=['Admin'], description='User payments')
class AdminPayments(MethodResource):
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        authenticate_admin(email)
        return Ledger.query.filter_by(type="payment").all()


