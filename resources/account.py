import os
import random
from datetime import datetime
import requests as rq
from sqlalchemy import and_, func
from flask_apispec import MethodResource, doc, marshal_with, use_kwargs as use_kwargs_doc
from flask_restful import abort
from flask_bcrypt import generate_password_hash
from webargs import fields, validate
from webargs.flaskparser import use_kwargs
from database import (Algorithm, Cube, db_session, Discount, Exchange, Invoice, Ledger,
                      ProductPrice, User, UserApiKey,
                      UserNotification, UserProduct)
from flask_jwt_extended import jwt_required, get_jwt_identity
from .tools import payment, resources
from .tools.account import delete_user, reset_user
from .tools.cube import (get_balance_data, asset_allocations_from_balances,
                         create_series_chart)
from schemas import (AlgorithmSchema, ExchangeSchema, InvoiceSchema, 
                        UserSchema, PaymentSchema)


_cryptobal_url = os.getenv('CRYPTOBAL_URL')
_email_api_url = os.getenv('EMAIL_URL')


# ----------------------------------------------- Account Resources

auth_args = {
    'password': fields.Str(required=True, description='Account password'),
    'otp_code': fields.Str(required=False, description='Second factor code'),
}


class Healthcheck(MethodResource):
    @doc(tags=['Healthcheck'], description='Endpoint for checking API health')
    def get(self):
        return {'message': 'coincube-back API is running'}


class AccountBalances(MethodResource):
    @jwt_required
    @doc(tags=['Account'], description='Retrieves combined account balances and current allocations.')
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        try:
            cubes = Cube.query.filter_by(
                            user_id=user.id
                            ).filter(and_(
                            Cube.closed_at == None,
                            func.length(Cube.balances) > 0,
                            )).all()
            if cubes:
                balances, total, performance_fiat = get_balance_data(cubes, user)
                allocations = asset_allocations_from_balances(balances)
                return {
                    'balances': balances, 
                    'allocations': allocations,
                    'total': total,
                    'performance_fiat': performance_fiat,
                    }
            else:
                return {}
        except:
            abort(500) 


class AccountValuations(MethodResource):
    @jwt_required
    @doc(tags=['Account'], 
        description='Retrieves individual BTC and fiat valuations for cubes/wallet.')
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        try:
            cubes = Cube.query.filter_by(
                            user_id=user.id
                            ).filter(
                            Cube.closed_at == None
                            ).all()
            if cubes:
                valuations = []
                for cube in cubes:
                    cube_valuation = {}
                    cube_totals = cube.get_performance()
                    if not cube_totals.empty:
                        vals = {}
                        vals['val_btc'] = round(cube_totals.btc_total[-1], 8)
                        vals['val_fiat'] = round(cube_totals.fiat_total[-1], 2)
                        if cube.name:
                            cube_valuation['name'] = cube.name
                        else:
                            name = cube.api_connections[0].exchange.name
                            cube_valuation['name'] = name
                        cube_valuation['values'] = vals
                        valuations.append(cube_valuation)
                return valuations
            else:
                return {}
        except:
            abort(500) 


class ApiKey(MethodResource):
    @jwt_required
    @use_kwargs(auth_args, locations=('json', 'form'))
    @use_kwargs_doc(auth_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Create API key to access account data')
    def post(self, password, otp_code):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Check password
        if user.social_id:
            password = user.social_id
        if not user.verify_password(password):
            abort(401, message='Wrong credentials.')
        # Create new key and return it
        key = hex(random.getrandbits(128))[2:-1] # Random 32 character key
        secret = hex(random.getrandbits(128))[2:-1] # Random 32 character secret
        secret_hash = generate_password_hash(secret)
        api_key = UserApiKey(
            user_id=user.id,
            key=key,
            secret=secret_hash
            )
        api_key.save_to_db()
        return {'key': key, 'secret': secret}

    post_args = {
        'key': fields.Str(required=True, description='API key to delete'),
    }
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Delete account API key')
    def delete(self, key):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        try:
            # Create new key and return it
            UserApiKey.query.filter_by(
                user_id=user.id,
                key=key
            ).delete()
            return {'message': 'API key deleted'}
        except:
            message = 'A problem was encountered while trying to delete your API key.'
            abort(400, message=message)


@marshal_with(AlgorithmSchema(many=True))
class AvailableAlgorithms(MethodResource):
    @doc(tags=['Account'], description='Returns available algorithms (also available in User object, may remove this route)')
    @jwt_required
    def get(self):
        return Algorithm.query.filter_by(active=True).all()


@marshal_with(ExchangeSchema(many=True))
class AvailableExchanges(MethodResource):
    @doc(tags=['Account'], description='Returns available exchanges for user (also available in user object, may remove this route')
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Get already used exchanges from user Cubes
        cubes = Cube.query.filter_by(user_id=user.id).all()
        # Exclude 12 (External)
        taken_exchanges = [12]
        for cube in cubes:
            for conn in cube.api_connections:
                taken_exchanges.append(conn.exchange.id)

        exchanges = Exchange.query.filter_by(active=True).filter(
                                ~Exchange.id.in_(taken_exchanges)
                                ).all()
        return exchanges


class BlockChainCallback(MethodResource):
    post_args = {
        'secret': fields.Str(required=True),
        'id': fields.Int(required=True),
        'value': fields.Int(required=True),
        'confirmations': fields.Int(required=True),
        'transaction_hash': fields.Str(required=True),
        'address': fields.Str(required=True),
    }
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Receives payment data from BlockChain.info wallet.')
    def post(self, **kwargs):
        return payment.blockchain_callback(kwargs)

class CoinPaymentsIPN(MethodResource):
    cp_ipn_secret = ''; 
    post_args = {
        'txn_id' :  fields.Str(required=False),
        'invoice': fields.Str(required=False),
        'amount1': fields.Str(required=False),
        'amount2': fields.Str(required=False),
        'currency1': fields.Str(required=False),
        'currency2': fields.Str(required=False),
        'status': fields.Str(required=False),
        'HTTP_HMAC': fields.Str(required=False),
    }
    @use_kwargs(post_args, locations=('json', 'form', 'headers'))
    @use_kwargs_doc(post_args, locations=('json', 'form', 'headers'))
    @doc(tags=['Account'], description='Receives payment data from CoinPayments IPN.')
    def post(self, **kwargs):
        return payment.coinpayments_ipn(kwargs)


class CoinPaymentsPayment(MethodResource):
    # only requires price_id OR months
    post_args = {
        'user_id': fields.Int(required=False, description='User ID'),
        'price_id': fields.Int(required=False, description='Product Price ID'),
        'months': fields.Int(required=False, description='Package Months'),
        'account_type': fields.Str(required=False, description='Account Type'),
        'discount_code': fields.Str(required=False, description='Discount Code'),
    }
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Retrieves an invoice id for user and selected package.')
    @jwt_required
    def post(self, user_id, price_id, months, account_type, discount_code):
        # either use a price id or determine from months and plan
        if not price_id and not months:
            abort(400, message="No package selected.")

        # if going by months must also have type, and vice versa
        if (months and not account_type) or (account_type and not months):
            abort(400, message="Both months and account_type required.")

        email = get_jwt_identity()
        current_user = User.query.filter_by(email=email).first()

        if user_id:
            user = User.query.get(user_id)
            resources.check_user_access(user, current_user)
        else: 
            user = current_user

        if not user:
            abort(403)

        if not price_id:
            months = payment.check_months(months) 
            product_price = payment.get_product_price(account_type, months)
        else:
            product_price = ProductPrice.query.get(price_id)
            if not product_price:
                abort(400, message="Product Price not found.")
            months = product_price.months


        amount =  months * float(product_price.monthly_rate)

        # Apply discount code to amount
        if discount_code:
            discount = Discount.query.filter_by(discount_code=discount_code).first()
            if discount:
                amount = amount * float(discount.percent)

        invoice = payment.get_open_invoice(user)

        if not invoice: #no open invoice found, create new
            invoice = Invoice(
                user_id = user.id,
                product_type = product_price.product_type,
                amount = amount,
                currency = "USD",
                months = months,
                product_price_id = product_price.id
            )

            db_session.add(invoice)
            db_session.commit()
            db_session.refresh(invoice)

        else: #use existing open invoice instead of creating new one
            if (invoice.amount != amount or 
                invoice.months != months or 
                invoice.product_type == product_price.product_type): 
                #udpate invoice to reflect current selections:
                invoice.amount = amount
                invoice.months = months
                invoice.product_type = product_price.product_type
                db_session.commit()

        return { "invoice_id" : invoice.id, "amount" : amount }


class PerformanceAccountResource(MethodResource):
    @jwt_required
    @doc(tags=['Account'], description='Returns aggregated balances and returns for Account')
    def get(self, timeframe):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        if user:
            try:
                dp = user.get_performance(timeframe=timeframe)

                # Return and balance charts
                fiat_pct_returns = create_series_chart(dp.fiat_pct.copy()) or []
                btc_pct_returns = create_series_chart(dp.btc_pct.copy()) or []
                fiat_balances = create_series_chart(dp.fiat_total.copy()) or []
                btc_balances = create_series_chart(dp.btc_total.copy()) or []

                return {
                    'fiat_pct_returns': fiat_pct_returns,
                    'btc_pct_returns': btc_pct_returns,
                    'fiat_balances': fiat_balances,
                    'btc_balances': btc_balances,
                    }
            except:
                return {}
        else:
            message = 'No user {}'.format(user.id)
            abort(404, message=message)


@marshal_with(InvoiceSchema(many=True))
@doc(tags=['Account'], description='User invoices')
class InvoiceResource(MethodResource):
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        return Invoice.query.filter_by(
                        user_id=user.id
                        ).filter(
                        Invoice.paid_at != None
                        ).all()


@marshal_with(PaymentSchema(many=True))
@doc(tags=['Account'], description='User payments')
class PaymentResource(MethodResource):
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        return Ledger.query.filter_by(user_id=user.id, type="payment").all()

class ProUpgradeCost(MethodResource):
    @doc(tags=['Account'], 
         description='Returns cost to upgrade basic to pro.')
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()

        amount = payment.get_pro_upgrade_price(user)
        if amount:
            return {'amount': amount}
        else:
            {'message': 'Not Index account.'}

class ProUpgradePayment(MethodResource):
    @doc(tags=['Account'], 
         description='Creates invoice for pro upgrade.')
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()                    

        if not user:
            abort(403)

        amount = payment.get_pro_upgrade_price(user)

        if amount:
            invoice = payment.get_open_invoice(user)

            if not invoice: #no open invoice found, create new
                invoice = Invoice(
                    user_id = user.id,
                    product_type = 'pro_upgrade',
                    amount = amount,
                    currency = 'USD'
                )

                db_session.add(invoice)
                db_session.commit()
                db_session.refresh(invoice)

            else: #use existing open invoice instead of creating new one
                invoice.amount = amount
                invoice.months = None
                invoice.product_type = 'pro_upgrade'
                invoice.product_price_id = None
                db_session.commit()

            return { 'invoice_id' : invoice.id, 'amount' : amount }

        else:
            return {'message': 'Not Index account.'}



class PaymentStatus(MethodResource):
    @doc(tags=['Account'], 
         description='Returns users current subscription status \
                     and info about subcriptions in general.')
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()

        user_product = UserProduct.query.filter_by(
            user_id=user.id,
        ).first()

        if not user_product:
            return {}

        # prepare response with user's current account/billing status
        data = {}
        if user_product:
            invoices = user_product.get_invoices()
            if user_product.paid_until > datetime.utcnow():
                paid_until = user_product.paid_until.timestamp()
            else:
                paid_until = None
            data['paid_until'] = paid_until
            data['last_payment'] =  invoices[0].paid_at.timestamp()
            data['product_name'] =  invoices[0].product_name

        return data


class SaveEmail(MethodResource):
    post_args = {**auth_args, **{
        'new_email': fields.Email(required=True, description='New email address'),
    }}
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Save new email address')
    def post(self, password, otp_code, new_email):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Confirm OTP is correct
        if user.otp_complete:
            if not user.verify_totp(otp_code):
                message = 'Your second factor code was invalid.'
                abort(403, message=message)
        # Check password
        elif not user.verify_password(password):
            message = 'Incorrect password. Try again.'
            abort(403, message=message)
        # Check to make sure email address isn't already in use
        if User.query.filter_by(email=new_email).first():
            message = 'Email address already in use. Please use another.'
            abort(403, message=message)
        
        # Commit new email address to database
        user.email = new_email
        user.save_to_db()
        return {'message': 'Email address was successfully changed.'}


class SavePassword(MethodResource):
    post_args = {**auth_args, **{
        'new_password': fields.Str(required=True, description='New password'),
    }}
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Save new password')
    def post(self, password, otp_code, new_password):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Confirm OTP is correct
        if user.otp_complete:
            if not user.verify_totp(otp_code):
                message = 'Your second factor code was invalid.'
                abort(403, message=message)
        # Check password
        elif not user.verify_password(password):
            message = 'Incorrect password. Try again.'
            abort(403, message=message)
        # Commit new email address to database
        user.password = new_password
        user.save_to_db()
        return {'message': 'Password was successfully changed.'}


class SaveSecondFactor(MethodResource):
    @jwt_required
    @use_kwargs(auth_args, locations=('json', 'form'))
    @use_kwargs_doc(auth_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Turn on Second Factor Authentication')
    def post(self, password, otp_code):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Confirm OTP is correct
        if not user.verify_totp(otp_code):
            message = 'Your second factor code was invalid.'
            abort(403, message=message)
        # Check password
        if user.social_id:
            password = user.social_id
        elif not user.verify_password(password):
            message = 'Incorrect password. Try again.'
            abort(403, message=message)            # Turn OTP on
        user.otp_complete = True
        user.save_to_db()
        return {'message': 'Second Factor Authentication enabled.'}


    @jwt_required
    @use_kwargs(auth_args, locations=('json', 'form'))
    @use_kwargs_doc(auth_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Turn off Second Factor Authentication')
    def delete(self, password, otp_code):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        # Confirm OTP is correct
        if user.otp_complete:
            if not user.verify_totp(otp_code):
                message = 'Your second factor code was invalid.'
                abort(403, message=message)
        # Check password
        if user.social_id:
            password = user.social_id
        elif not user.verify_password(password):
            message = 'Incorrect password. Try again.'
            abort(403, message=message)
        # Turn OTP on
        user.otp_complete = False
        user.save_to_db()
        return {'message': 'Second Factor Authentication disabled.'}


class SecondFactorSecret(MethodResource):
    @jwt_required 
    @doc(tags=['Account'], description='Secret for Second Factor Authentication')
    def get(self):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        try:
            if not user.otp_complete:
                user.set_totp_secret()
            return {'secret': user.otp_secret}
        except:
            message = 'A problem was encountered.'
            abort(400, message=message)
            

class SaveUserSetting(MethodResource):
    post_args = {
        'name': fields.Str(required=True, description='Setting name'),
        'value': fields.Str(required=True, description='Setting value'),
    }
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], 
        description='Save user setting. "name"=("btc_data", "wide_charts", "portfolio", \
            "fiat_id", "first_name", "delete_notification", "reset_user", "delete"), \
        "value"=("true/false", "true/false", "true/false", "int", "str", "int", none, none)')
    def post(self, name, value):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()

        try:
            bool_value = 1 if value == "true" else 0
            if name in ["btc_data", "wide_charts", "portfolio"]:
                setattr(user, name, bool_value)
            elif name in ["fiat_id"]:
                setattr(user, name, value)
            elif name in ["news", "alerts"]:
                setattr(user, name, bool_value)
                user.save_to_db()
            elif name in ["first_name"]:
                setattr(user, name, value)
                user.save_to_db()
            elif name in ["delete_notification"]:
                UserNotification.query.filter_by(id=name).delete()
            elif name in ["reset_user"]:
                reset_user(user.id)
            elif name in ["delete"]:
                delete_user(user.id)
                return {'message': "user deleted, route to landing page"}
            ### TO DO
            # Need to kill JWT tokens and send to landing page
            return {'message': 'Setting successfully saved'}
        except Exception as e:
            message = 'A problem was encountered.'
            abort(400, message=message)


class SendSupportEmail(MethodResource):
    post_args = {
        'subject': fields.Str(required=True, description='Email subject'),
        'message': fields.Str(required=True, description='User support message')
    }
    @jwt_required
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Account'], description='Send support email to support@coincube.io')
    def post(self, subject, message):
        email = get_jwt_identity()
        user = User.query.filter_by(email=email).first()
        try:
            if user.is_pro:
                account_type = 'Pro'
            elif user.is_basic:
                account_type = 'Basic'
            else:
                account_type = 'Free'
            data = {
                'subject': subject,
                'username': user.first_name,
                'email': user.email,
                'account_type': account_type,
                'message': message
            }
            url = _email_api_url + '/support_request'
            r = rq.post(url, data=data)
            message = 'Support email successfully sent'
            if r.status_code == 200:
                return {'message': message}
            else:
                abort(r.status_code)
        except:
            message = 'A problem was encountered while trying to send the support email.'
            abort(400, message=message)


@marshal_with(UserSchema())
@doc(tags=['Account'], description='User object')
class UserResource(MethodResource):
    @jwt_required
    def get(self):
        email = get_jwt_identity()
        return User.query.filter_by(email=email).first()


