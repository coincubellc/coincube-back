import json
import requests
from rauth import OAuth1Service, OAuth2Service
from coinbase.wallet.client import OAuthClient
from flask import current_app, request, redirect, session

class OAuthSignIn(object):
    providers = None

    def __init__(self, provider_name):
        self.provider_name = provider_name
        credentials = current_app.config['OAUTH_CREDENTIALS'][provider_name]
        self.consumer_id = credentials['id']
        self.consumer_secret = credentials['secret']

    def authorize(self):
        pass

    def callback(self):
        pass

    def get_callback_url(self):
        return 'http://localhost:443/oauth/callback/<{}>'.format(self.provider_name)
        # return api.url_for(OauthCallback, 
        #                provider=self.provider_name,
        #                _external=True)

    @classmethod
    def get_provider(self, provider_name):
        if self.providers is None:
            self.providers = {}
            for provider_class in self.__subclasses__():
                provider = provider_class()
                self.providers[provider.provider_name] = provider
        return self.providers[provider_name]


class FacebookSignIn(OAuthSignIn):
    def __init__(self):
        super(FacebookSignIn, self).__init__('facebook')
        self.service = OAuth2Service(
            name='facebook',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://graph.facebook.com/oauth/authorize',
            access_token_url='https://graph.facebook.com/oauth/access_token',
            base_url='https://graph.facebook.com/'
        )

    def authorize(self):
        return redirect(self.service.get_authorize_url(
            scope='email',
            response_type='code',
            redirect_uri=self.get_callback_url()),
            code=301
        )

    def callback(self):
        if 'code' not in request.args:
            return None, None, None
        oauth_session = self.service.get_auth_session(
            data={'code': request.args['code'],
                  'grant_type': 'authorization_code',
                  'redirect_uri': self.get_callback_url()
            },
            decoder=json.loads
        )
        me = oauth_session.get('me?fields=id,email,first_name,last_name').json()
        social_id = 'facebook$' + me['id']
        if 'email' not in me:
            return social_id, me.get('first_name'), social_id
        else:
            return (
                social_id,
                me.get('first_name'),
                me.get('email'),
            )

class CoinbaseSignIn(OAuthSignIn):
    def __init__(self):
        super(CoinbaseSignIn, self).__init__('coinbase')
        self.service = OAuth2Service(
            name='coinbase',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://www.coinbase.com/oauth/authorize',
            access_token_url='https://www.coinbase.com/oauth/token',
            base_url='https://www.coinbase.com/oauth/'
        )

    def authorize(self):
        print(self.get_callback_url())
        return redirect(self.service.get_authorize_url(
            scope='wallet:user:read,wallet:accounts:read,wallet:user:email,'\
                   'wallet:accounts:update,wallet:accounts:create,'\
                   'wallet:buys:read,wallet:buys:create,'\
                   'wallet:sells:read,wallet:sells:create',
            response_type='code',
            redirect_uri=self.get_callback_url())
        )

    def get_tokens(self, code, grant_type='authorization_code'):
        params = {
            'grant_type': grant_type,
            'redirect_uri': self.get_callback_url(),
            'client_id': self.consumer_id,
            'client_secret': self.consumer_secret
        }

        if grant_type == 'refresh_token':
            params['refresh_token'] = str(code)
            print(params['refresh_token'])
            print(params['grant_type'])
            request = \
                    requests.post(
                        'https://api.coinbase.com/oauth/token',
                        data=params)
        else:
            params['code'] = code

            request = \
                requests.post(
                    'https://coinbase.com/oauth/token',
                    data=params)

        if request.status_code != 200:
            raise

        return json.loads(request.content.decode())


    def refresh_tokens(self, refresh_token):
        return self.get_tokens(refresh_token, grant_type='refresh_token')

    def callback(self):
        if 'code' not in request.args:
            return None, None
        # Get code
        code = request.args.get('code')
        # Get tokens
        response = self.get_tokens(code)
        access_token = response['access_token']
        refresh_token = response['refresh_token']
        # Create client
        client = OAuthClient(access_token, refresh_token)
        # Create new COINCUBE wallet at Coinbase
        try:
            res = client.create_account(name="COINCUBE")
            print(res)
            wallet_id = res.id
        except:
            wallet_id = None

        return wallet_id, refresh_token

class TwitterSignIn(OAuthSignIn):
    def __init__(self):
        super(TwitterSignIn, self).__init__('twitter')
        self.service = OAuth1Service(
            name='twitter',
            consumer_key=self.consumer_id,
            consumer_secret=self.consumer_secret,
            request_token_url='https://api.twitter.com/oauth/request_token',
            authorize_url='https://api.twitter.com/oauth/authorize',
            access_token_url='https://api.twitter.com/oauth/access_token',
            base_url='https://api.twitter.com/1.1/'
        )

    def authorize(self):
        request_token = self.service.get_request_token(
            params={'oauth_callback': self.get_callback_url()}
        )
        session['request_token'] = request_token
        return redirect(self.service.get_authorize_url(
                            request_token[0]), 
                            code=301
                        )

    def callback(self):
        request_token = session.pop('request_token')
        if 'oauth_verifier' not in request.args:
            return None, None, None
        oauth_session = self.service.get_auth_session(
            request_token[0],
            request_token[1],
            data={'oauth_verifier': request.args['oauth_verifier']}
        )
        me = oauth_session.get('account/verify_credentials.json', params={"include_email" : "true"}).json()
        print(me)
        social_id = 'twitter$' + str(me.get('id'))
        username = me.get('screen_name')
        try:
            email = me.get('email')
        except:
            email = None
        return social_id, username, email