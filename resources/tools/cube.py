import os
import math
from time import sleep
from datetime import datetime, timedelta
import pandas as pd
import requests as rq
from flask_restful import abort
from database import (and_, app, AssetAllocation, Balance, Connection, 
                      Currency, db_session, e,
                      Exchange, ExPair, ExternalAddress, ExternalTransaction, 
                      func, IndexPair, or_, Transaction)


API_RETRIES = 3  # Times to retry query before giving up
GRACE_TIME = 5 # Seconds to sleep on exception

_cryptobal_url = os.getenv('CRYPTOBAL_URL')
_exapi_url = os.getenv('EXAPI_URL')


def asset_allocations(cube):
    # Current asset allocations
    asset_alls = AssetAllocation.query.filter_by(
                    cube_id=cube.id).join(Currency, 
                    AssetAllocation.currency).order_by(
                    Currency.symbol).all()
    if asset_alls:
        assets = []
        for asset in asset_alls:
            if asset.percent:
                assets.append({'name': asset.currency.symbol,
                               'y': float(asset.percent)})              
    else:
        assets = None
    return assets


def asset_allocations_set(cube, allocations):
    symbols = []
    total = 0

    for a in allocations:
        total += a['y']

    for a in allocations:

        symbols.append(a['name'])
        currency = Currency.query.filter_by(symbol=a['name']).first()
        allocation = AssetAllocation.query.filter_by(cube_id=cube.id, currency=currency).first()

        if allocation: # update existing allocation
            allocation.percent = a['y'] / total
        else: # save new allocation 
            new_allocation = AssetAllocation(
                            cube_id = cube.id,
                            currency = currency,
                            percent = a['y'] / total
                        )

            new_allocation.save_to_db()

    # zero out old allocations
    for symbol in cube.allocations:
        if not symbol in symbols:
            allocation = AssetAllocation.query.join(Currency).filter(
                                AssetAllocation.cube_id==cube.id, 
                                Currency.symbol==symbol
                            ).first()
            allocation.percent = 0

    # Update cube.reallocated_at
    cube.reallocated_at = datetime.utcnow()
    cube.save_to_db()

    # Log allocation changes
    cube.log_user_action("Portfolio updated", str(allocations))
                
    return {'message': 'Allocations successfully updated'}

def asset_allocations_from_balances(balances, cube=None):
    if cube:
        # Create allocations from balances if no allocations
        alloc = AssetAllocation.query.filter_by(cube_id=cube.id).first()
        if not alloc:
            for bal in balances['values']:
                cur = Currency.query.filter_by(symbol=bal[0]).first()
                a = AssetAllocation(
                        cube_id=cube.id,
                        currency_id=cur.id,
                        percent=float(bal[8])
                    )
                db_session.add(a)
            db_session.commit()
    # Create asset allocations from balances
    # Either when allocations are missing 
    # Or when set to Tracker account
    assets = []
    for bal in balances['values']:
        if bal[8] > 0:
            assets.append({'name': bal[0],
                               'y': float(bal[8])}) 
    return assets 


def create_series_chart(df):
    df.index = (df.index.values.astype(float) / 1000000).astype(float)
    return [list(p) for p in df.iteritems() if not math.isnan(p[1])]


def get_external_balances(address, address_type):
    url = f"{_cryptobal_url}/{address_type}/{address}" 
    for i in range(API_RETRIES + 1):
        try:
            r = rq.get(url)
            r.raise_for_status()
            if r.status_code == 200:
                json_content = r.json()
                return json_content
        except Exception as e:
            if i == API_RETRIES:
                return None
            sleep(GRACE_TIME)


def get_all_external_balances(cube): 
    bals = {}
    # check all addresses
    for ea in cube.external_addresses:

        ret = get_external_balances(ea.address, ea.address_type)
        if not ret:
            return None

        if 'balances' in ret:
            ret = ret['balances']
        else:
            return None

        # ignore non-ascii symbols
        for sym in tuple(ret):
            try:
                sym.encode('ascii')
            except UnicodeEncodeError:
                del ret[sym]
        ea.updated_at = datetime.utcnow()
        db_session.add(ea)
        db_session.commit()

        # add address symbol balances
        for sym, bal in ret.items():
            bal = int(float(bal) * 1e8) / 1e8
            if sym not in bals:
                bals[sym] = {'available': 0, 'reserved': 0, 'total': 0}

            bals[sym]['reserved'] += bal
            bals[sym]['total'] += bal
            bals[sym]['available'] += bal

            # draft new transaction
            etx = ExternalTransaction(
                cube=cube,
                address=ea,
                symbol=sym,
                balance=bal
                )
            db_session.add(etx)
            db_session.commit()

    # Return none if all external addresses have 0 balances
    if not bals:
        return None

    # Add LTC if missing # Needed for stand-in base for BTC as quote
    if 'LTC' not in bals:
        bals['LTC'] = {'available': 0, 'reserved': 0, 'total': 0}
    # Add BTC if missing
    if 'BTC' not in bals:
        bals['BTC'] = {'available': 0, 'reserved': 0, 'total': 0}  
    return bals


def get_balance_data(cubes, user):
    app.logger.debug("Start get_balance_data()")
    app.logger.debug(datetime.utcnow())
    # Child rows for per exchange balances
    bals = [(b.currency.symbol, b.cube.name, float(b.total), b.index_pair) for cube in cubes
            for b in cube.balances if (b.total > 9e-8) or (b.currency.symbol == 'BTC')]
    app.logger.debug("Retrieved balances")
    app.logger.debug(datetime.utcnow())
    bals = pd.DataFrame(bals, columns=['Asset', 'Exchange', 'Balance', 'IndexPair'])
    bals['1d'] = bals.IndexPair.map(lambda pair: pair.get_days_performance(1) if pair else 0)
    bals['1w'] = bals.IndexPair.map(lambda pair: pair.get_days_performance(7) if pair else 0)
    bals['1m'] = bals.IndexPair.map(lambda pair: pair.get_days_performance(30) if pair else 0)
    performance = {
        "1d": bals.pivot(index='Asset', columns='Exchange', values='1d'),
        "1w": bals.pivot(index='Asset', columns='Exchange', values='1w'),
        "1m": bals.pivot(index='Asset', columns='Exchange', values='1m')
    }
    app.logger.debug("Retrieved days performance")
    app.logger.debug(datetime.utcnow())
    bals = bals.pivot(index='Asset', columns='Exchange', values='Balance')
    # # Multiply the performance by the exchanges to get a weighed performance value
    performance = {"1d": performance["1d"] * bals, "1w": performance["1w"] * bals, "1m": performance["1m"] * bals}
    bals['Total'] = bals.sum(axis=1)
    # Divide by the total to get the actual performance
    performance = {
        "1d": performance["1d"].sum(axis=1).divide(bals['Total']),
        "1w": performance["1w"].sum(axis=1).divide(bals['Total']),
        "1m": performance["1m"].sum(axis=1).divide(bals['Total'])
    }
    bals = bals.fillna('-')
    bals = bals.applymap(lambda x: '%.8f' % x if type(x) is float else x)

    total_bals = bals.Total
    total_bals = total_bals.to_frame()
    bals = bals.reset_index()

    # Total bals and full dataset
    performance_fiat = {"1d": None, "1w": None, "1m": None}
    # Get asset prices in BTC
    app.logger.debug("Get BTC rates")
    app.logger.debug(datetime.utcnow())
    app.logger.debug("User fiat symbol")
    app.logger.debug(user.fiat.symbol)
    btc_rate = []
    selected_btc_fiat = None
    for symbol in total_bals.index:
        app.logger.debug(symbol)
        # Get ex_pair
        if symbol == 'BTC':
            btc_rate.append(1)
        else:
            cur = Currency.query.filter_by(symbol=symbol).first()
            if not cur:
                btc_rate.append(0)
                continue
            index_pair = IndexPair.query.filter_by(quote_currency_id=2,
                                                   base_currency_id=cur.id,
                                                   active=True).first()

            if not index_pair:
                index_pair = IndexPair.query.filter_by(quote_symbol=symbol,
                                                       base_currency_id=2,
                                                       active=True).first()

                if not index_pair:
                    btc_rate.append(0)
                    continue

                try:
                    btc_fiat = index_pair.index_pair_close[0].close
                except:
                    btc_fiat = 0
                performance_fiat = {
                    "1d": performance["1d"].loc[index_pair.quote_currency.symbol],
                    "1w": performance["1w"].loc[index_pair.quote_currency.symbol],
                    "1m": performance["1m"].loc[index_pair.quote_currency.symbol]
                }
                if btc_fiat > 0:
                    rate = round(1 / btc_fiat, 8)
                else:
                    rate = 0
                btc_rate.append(rate)
                if symbol == user.fiat.symbol:
                    selected_btc_fiat = btc_fiat
            else:
                try:
                    rate = index_pair.index_pair_close[0].close
                except:
                    rate = 0
                btc_rate.append(rate)
            app.logger.debug(index_pair)
    app.logger.debug("Retrieved BTC rates")
    app.logger.debug(datetime.utcnow())
    app.logger.debug(btc_rate)

    # Find btc_fiat rate if not in bals
    if not selected_btc_fiat:
        fiat = IndexPair.query.filter_by(
            base_symbol='BTC',
            quote_symbol=user.fiat.symbol
        ).first()
        performance_fiat = {
            "1d": fiat.get_days_performance(1),
            "1w": fiat.get_days_performance(7),
            "1m": fiat.get_days_performance(30)
        }
        try:
            selected_btc_fiat = fiat.index_pair_close[0].close
        except:
            selected_btc_fiat = 0

    app.logger.debug("Retrieved fiat rate")
    app.logger.debug(datetime.utcnow())
    app.logger.debug(selected_btc_fiat)

    # BTC rate
    total_bals['BTC_Rate'] = btc_rate
    total_bals[['Total', 'BTC_Rate']] = total_bals[['Total', 'BTC_Rate']].astype(float)
    # Value in BTC
    total_bals['BTC_Value'] = total_bals.Total.multiply(total_bals.BTC_Rate)
    # Value in Fiat
    total_bals['Fiat_Value'] = total_bals.BTC_Value.multiply(float(selected_btc_fiat))
    # Performance
    total_bals['1d_BTC_Change'] = performance["1d"]
    total_bals['1w_BTC_Change'] = performance["1w"]
    total_bals['1m_BTC_Change'] = performance["1m"]
    app.logger.debug("Retrieved balances")
    app.logger.debug(datetime.utcnow())
    app.logger.debug(total_bals)
    # Percentage of portfolio
    total_btc = total_bals.BTC_Value.sum()
    total_bals['Percent_of_Portfolio'] = total_bals.BTC_Value.divide(total_btc)

    # Percent Off Goal
    def target_mapper(bal):
        for cube in cubes:
            if bal.name in cube.allocations:
                return float(cube.allocations[bal.name].percent)
            else:
                return 0

    total_bals['Target'] = total_bals.apply(target_mapper, axis=1)

    # Percent Off Goal
    def percent_off_mapper(bal):
        for cube in cubes:
            if bal.name in cube.allocations:
                target = float(cube.allocations[bal.name].percent)
            else:
                target = 0
            if target == 0:
                return 0
            return (bal.Percent_of_Portfolio - target) / target

    total_bals['Percent_Off_Goal'] = total_bals.apply(percent_off_mapper, axis=1)

    # BTC and fiat total
    total_btc = total_bals.BTC_Value.sum()
    total_fiat = total_bals.Fiat_Value.sum()

    # Filter out balances with fiat value < 1
    total_bals = total_bals[total_bals.Fiat_Value >= 1].copy()

    header = total_bals.columns.tolist()
    total_bals = total_bals.reset_index()
    total_bals = total_bals.fillna(0)
    values = total_bals.values.tolist()

    # Balances
    balances = {}
    balances['header'] = header or []
    balances['values'] = values or []

    # Totals
    total = {}
    total['btc'] = total_btc or 0
    total['fiat'] = total_fiat or 0
    app.logger.debug("End get_balance_data()")
    app.logger.debug(datetime.utcnow())
    return balances, total, performance_fiat


def get_balance_data_new(cube, user):
    # List comprehension to construct balances info
    bals = [(b.currency.symbol,
             float(b.total),
             float(b.btc_rate),
             b.currency.percent_change_1h,
             b.currency.percent_change_24h,
             b.currency.percent_change_7d)
            for b in cube.balances if (b.ex_pair.active and (b.total > 9e-8) or (b.currency.symbol == 'BTC'))]

    # Create Dataframe
    bals = pd.DataFrame(bals, columns=['Asset', 'Balance', 'BTC_Rate', '1h_BTC_Change', '1d_BTC_Change', '1w_BTC_Change'])
    bals = bals.fillna('-')

    # Value in BTC
    bals['BTC_Value'] = bals.Balance.multiply(bals.BTC_Rate)

    # Value in Fiat
    fiat = IndexPair.query.filter_by(
        base_symbol='BTC',
        quote_symbol=user.fiat.symbol
    ).first()
    try:
        selected_btc_fiat = fiat.index_pair_close[0].close
    except:
        selected_btc_fiat = 0
    bals['Fiat_Value'] = bals.BTC_Value.multiply(float(selected_btc_fiat))

    # Percentage of portfolio
    total_btc = bals.BTC_Value.sum()
    bals['Percent_of_Portfolio'] = bals.BTC_Value.divide(total_btc)

    # Percent Off Goal
    def target_mapper(bal):
        if bal.Asset in cube.allocations:
            return float(cube.allocations[bal.Asset].percent)
        else:
            return 0

    bals['Target'] = bals.apply(target_mapper, axis=1)

    # Percent Off Goal
    def percent_off_mapper(bal):
        if bal.Asset in cube.allocations:
            target = float(cube.allocations[bal.Asset].percent)
        else:
            target = 0
        if target == 0:
            return 0
        return (bal.Percent_of_Portfolio - target) / target

    bals['Percent_Off_Goal'] = bals.apply(percent_off_mapper, axis=1)

    # BTC and fiat total
    total_btc = bals.BTC_Value.sum()
    total_fiat = bals.Fiat_Value.sum()

    # Filter out balances with fiat value < 1
    bals = bals[bals.Fiat_Value >= 1].copy()

    print(bals)
    print(bals['1h_BTC_Change'])
    print(bals['1d_BTC_Change'])
    print(bals['1w_BTC_Change'])

    header = bals.columns.tolist()
    bals = bals.fillna(0)
    values = bals.values.tolist()

    # Balances
    balances = {}
    balances['header'] = header or []
    balances['values'] = values or []

    # Totals
    total = {}
    total['btc'] = total_btc or 0
    total['fiat'] = total_fiat or 0

    return balances, total


def get_total_balance_data(cubes, user):

    # List comprehension to construct balances info
    # bals = [(b.currency.symbol,
    #          b.cube.name,
    #          float(b.total),
    #          float(b.btc_rate),
    #          b.currency.percent_change_1h,
    #          b.currency.percent_change_24h,
    #          b.currency.percent_change_7d)
    #         for b in cube.balances if (b.ex_pair.active and (b.total > 9e-8) or (b.currency.symbol == 'BTC'))]
    #
    # # Create Dataframe
    # bals = pd.DataFrame(bals, columns=['Asset', 'Exchange', 'Balance', 'BTC_Rate', '1h_BTC_Change', '1d_BTC_Change', '1w_BTC_Change'])
    # bals = bals.fillna('-')

    # Child rows for per exchange balances
    bals = [(b.currency.symbol, b.cube.name, float(b.total), b.currency) for cube in cubes
            for b in cube.balances if (b.total > 9e-8) or (b.currency.symbol == 'BTC')]
    bals = pd.DataFrame(bals, columns=['Asset', 'Exchange', 'Balance', 'Currency'])
    bals['1h'] = bals.Currency.map(lambda cur: float(cur.percent_change_1h) if cur.percent_change_1h else 0)
    bals['1d'] = bals.Currency.map(lambda cur: float(cur.percent_change_24h) if cur.percent_change_24h else 0)
    bals['1w'] = bals.Currency.map(lambda cur: float(cur.percent_change_7d) if cur.percent_change_7d else 0)
    performance = {
        "1h": bals.pivot(index='Asset', columns='Exchange', values='1h'),
        "1d": bals.pivot(index='Asset', columns='Exchange', values='1d'),
        "1w": bals.pivot(index='Asset', columns='Exchange', values='1w')
    }
    bals = bals.pivot(index='Asset', columns='Exchange', values='Balance')
    # # Multiply the performance by the exchanges to get a weighed performance value
    performance = {"1h": performance["1h"] * bals, "1d": performance["1d"] * bals, "1w": performance["1w"] * bals}
    bals['Total'] = bals.sum(axis=1)
    # Divide by the total to get the actual performance
    performance = {
        "1h": performance["1h"].sum(axis=1).divide(bals['Total']),
        "1d": performance["1d"].sum(axis=1).divide(bals['Total']),
        "1w": performance["1w"].sum(axis=1).divide(bals['Total'])
    }
    bals = bals.fillna('-')
    # bals = bals.applymap(lambda x: '%.8f' % x if type(x) is float else x)

    total_bals = bals.Total
    total_bals = total_bals.to_frame()
    # bals = bals.reset_index()

    # Total bals and full dataset
    # Get asset prices in BTC
    btc_rate = []
    selected_btc_fiat = None
    for symbol in total_bals.index:
        app.logger.debug(symbol)
        # Get ex_pair
        if symbol == 'BTC':
            btc_rate.append(1)
        else:
            cur = Currency.query.filter_by(symbol=symbol).first()
            if not cur:
                 btc_rate.append(0)
                 continue
            index_pair = IndexPair.query.filter_by(quote_currency_id=2,
                                             base_currency_id=cur.id, 
                                             active=True).first()

            if not index_pair:
                index_pair = IndexPair.query.filter_by(quote_symbol=symbol,
                                                 base_currency_id=2,
                                                 active=True).first()

                if not index_pair:
                    btc_rate.append(0)
                    continue

                try:
                    btc_fiat = index_pair.index_pair_close[0].close
                except:
                    btc_fiat = 0

                if btc_fiat > 0:
                    rate = round(1/btc_fiat, 8)
                else:
                    rate = 0
                btc_rate.append(rate)

                if symbol == user.fiat.symbol:
                    selected_btc_fiat = btc_fiat
            else:
                try:
                    rate = index_pair.index_pair_close[0].close
                except:
                    rate = 0
                btc_rate.append(rate)

    # Find btc_fiat rate if not in bals
    if not selected_btc_fiat:
        fiat = IndexPair.query.filter_by(
                    base_symbol='BTC',
                    quote_symbol=user.fiat.symbol
                    ).first()
        try:
            selected_btc_fiat = fiat.index_pair_close[0].close
        except:
            selected_btc_fiat = 0

    # BTC rate
    total_bals['BTC_Rate'] = btc_rate
    total_bals[['Total', 'BTC_Rate']] = total_bals[['Total', 'BTC_Rate']].astype(float)
    # Value in BTC
    total_bals['BTC_Value'] = total_bals.Total.multiply(total_bals.BTC_Rate)
    # Value in Fiat
    total_bals['Fiat_Value'] = total_bals.BTC_Value.multiply(float(selected_btc_fiat))
    # Performance
    total_bals['1h_BTC_Change'] = performance["1h"]
    total_bals['1d_BTC_Change'] = performance["1d"]
    total_bals['1w_BTC_Change'] = performance["1w"]

    # Percentage of portfolio
    total_btc = total_bals.BTC_Value.sum()
    total_bals['Percent_of_Portfolio'] = total_bals.BTC_Value.divide(total_btc)

    # BTC and fiat total
    total_btc = total_bals.BTC_Value.sum()
    total_fiat = total_bals.Fiat_Value.sum()

    # Filter out balances with fiat value < 1
    total_bals = total_bals[total_bals.Fiat_Value >= 1].copy()

    header = total_bals.columns.tolist()
    total_bals = total_bals.reset_index()
    total_bals = total_bals.fillna(0)
    values = total_bals.values.tolist()

    # Balances
    balances = {}
    balances['header'] = header or []
    balances['values'] = values or []

    # Totals
    total = {}
    total['btc'] = total_btc or 0
    total['fiat'] = total_fiat or 0

    return balances, total


def get_ex_assets(cube):
    ex_assets = {}
    for c in cube.connections.values():
        if not c.failed_at:
            asset_list = []
            ex_pairs = ExPair.query.filter_by(
                            exchange_id=c.exchange_id).all()
            for pair in ex_pairs:
                if pair.quote_currency.symbol not in asset_list:
                    asset_list.append(pair.quote_currency.symbol)
                if pair.base_currency.symbol not in asset_list:
                    asset_list.append(pair.base_currency.symbol)
            ex_assets[c.exchange.name] = asset_list

    return ex_assets


def get_fiat_allocation(cube):
    if (cube.allocations and 
        cube.fiat.symbol in cube.allocations and not 
        cube.allocations[cube.fiat.symbol] is None):
        fiat_allocation = int(cube.allocations[cube.fiat.symbol].percent)
    else:
        fiat_allocation = 0
    return fiat_allocation


def get_request(endpoint, exchange, key, secret, passphrase=None):
    for i in range(API_RETRIES + 1):
        try:
            url = _exapi_url + '/' + exchange + endpoint
            app.logger.debug(url)
            params = {'key' : key, 'secret' : secret, 'passphrase': passphrase}
            r = rq.get(url, params=params)
            app.logger.debug(r.url)
            app.logger.debug(r.status_code)
            r.raise_for_status()
            if r.status_code == 200:
                json_content = r.json()
                app.logger.debug(json_content)
                return json_content
        except Exception as e:
            if i == API_RETRIES:
                return None
            app.logger.exception(e)
            sleep(GRACE_TIME)


def process_key_exception(e, ex_id, cube):
    err = e.args[0]
    if type(err).__name__ == 'APIError':
        app.logger.exception(err)
    elif type(err).__name__ == 'APIKeyError':
        app.logger.exception('%s Invalid API key/secret for Ex_ID %s: %s' % (str(cube), ex_id, err))
    elif type(err).__name__ == 'ConnectionError':
        app.logger.exception('%s Invalid API key/secret for Ex_ID %s: %s' % (str(cube), ex_id, err))
    else:
        app.logger.exception('[%s] %s' % (cube, err))
    return False


def add_external_address(cube, address, blockchain):
    app.logger.debug('adding external address')
    cur = Currency.query.filter_by(name=blockchain).first()
    # Ensure address is valid before proceeding
    data = get_external_balances(address, cur.symbol)
    if data['address_type'] == 'unknown':
        abort(400, message="Unsupported blockchain or malformed address")
    cur = Currency.query.filter_by(name=blockchain).first()
    ex_address = ExternalAddress(
            cube_id=cube.id, 
            address=address,
            address_type=cur.symbol
            )
    ex_address.save_to_db()
    ex = Exchange.query.filter_by(name="External").first()
    ex_id = ex.id
    key = cube.id
    # Check for existing balances
    bals = Balance.query.filter_by(cube_id=cube.id, exchange_id=ex_id).all()
    if bals:
        add_txs(ex_id, cube, ttype='withdraw')
        remove_balances(ex_id, cube)
    if add_balances(ex_id, key, key, None, cube):
        app.logger.debug('adding balances')
        if add_txs(ex_id, cube):
            external_conn = Connection.query.filter_by(
                                        cube_id=cube.id,
                                        exchange_id=ex_id
                                        ).first()
            if not external_conn:
                conn = Connection(
                    user_id=cube.user_id,
                    cube_id=cube.id,
                    exchange_id=ex_id,
                    key=key,
                    secret=key,
                    passphrase=None
                    )
                conn.save_to_db()
            app.logger.info('[%s] Added external address: %s' % (cube, address))
            cube.log_user_action("add_external_address")
            cube.update_charts = 1
            cube.save_to_db()
            return 'Address added successfully'
        else:
            remove_external_address(cube, address)
            app.logger.warning('[%s] Problem adding external address: %s' % (cube, address))
            message = 'There was a problem adding this address'
            abort(400, message=message)
    else:
        remove_external_address(cube, address)
        message = 'We do not currently support address tracking for this blockchain'
        abort(400, message=message)


def remove_external_address(cube, address):
    app.logger.debug('remove external address')
    try:
        ex = Exchange.query.filter_by(name="External").first()
        ex_id = ex.id
        key = cube.id
        # Add withdraw tx and remove balances
        add_txs(ex_id, cube, ttype='withdraw')
        remove_balances(ex_id, cube)
        external_address = ExternalAddress.query.filter_by(cube_id=cube.id, address=address).first()
        db_session.delete(external_address)
        db_session.commit()
        if add_balances(ex_id, key, key, None, cube):
            add_txs(ex_id, cube, ttype='deposit')
            remaining_addresses = ExternalAddress.query.filter_by(cube_id=cube.id).all()
            if not remaining_addresses:
                conn = Connection.query.filter_by(
                        cube_id=cube.id,
                        exchange_id=ex_id).first()
                app.logger.debug('[%s] Removing connection for Ex_ID: %s' % (cube, ex_id))
                db_session.delete(conn)
                db_session.commit()
            app.logger.info('[%s] Removed external address: %s' % (cube, address))
            cube.log_user_action("remove_external_address")
            cube.update_charts = 1
            cube.save_to_db()
            return 'Address removed successfully'
        
    except Exception as e:
        app.logger.debug(e)
        abort(500)


def test_key(cube, ex_id, key, secret, passphrase):
    ex_name = Exchange.query.filter_by(id=ex_id).first().name
    # Check for balances
    try:
        bals = get_request('/balances', ex_name, key, secret, passphrase)
        app.logger.debug(bals)
    except Exception as e:
        message = 'Exception while querying balances.'
        return process_key_exception(e, ex_id, cube), message

    if not bals:
        app.logger.warning('No balances for Ex_ID %s' % (ex_id))
        message = 'You have no balances. Please deposit funds and try again.'
        return False, message

    # Look for non-zero balances for Coinbase Pro accounts
    for bal in bals.values():
        if bal['total']:
            break  
    else:
        message = 'You have no balances. Please deposit funds and try again.'
        return False, message

    # Check for trade permission
    try:
        if not get_request('/trade/test', ex_name, key, secret, passphrase):
            message = 'API key trading is not enabled.'
            return False, message
    except Exception as e:
        message = 'Exception while querying trading permissions.'
        return process_key_exception(e, ex_id, cube), message

    # Test for withdrawal permission
    try:
        if get_request('/withdrawal/test', ex_name, key, secret, passphrase):
            message = 'API keys have withdrawal/transfer enabled. Please remove this permission and try again.'
            return False, message
    except Exception as e:
        message = 'Exception while querying withdrawal permissions.'
        return process_key_exception(e, ex_id, cube), message

    # All is correct, return True
    return True, 'Keys are configured correctly.'


def add_key(cube, ex_id, key, secret, passphrase):
    app.logger.debug('[%s] Adding keys' % (cube))
    # Add initial transactions to database
    if add_balances(ex_id, key, secret, passphrase, cube):
        if add_txs(ex_id, cube):
            try:
                # Encrypt keys and add connection
                conn = Connection(
                    user_id=cube.user_id,
                    cube_id=cube.id,
                    exchange_id=ex_id,
                    key=e(key),
                    secret=e(secret),
                    passphrase=e(passphrase)
                    )
                conn.save_to_db()
                app.logger.info('[%s] Added API key for Ex_ID: %s' % (cube, ex_id))
                message = 'API keys added successfully. Exchange connection is live!'
                cube.log_user_action("save_api_keys")
                cube.update_charts = 1
                cube.save_to_db()
                return message
            except Exception as error:
                app.logger.debug('[%s] Trouble adding API key for Ex_ID: %s' % (cube, ex_id))
                app.logger.debug(error)
                remove_balances(ex_id, cube)
                remove_all_txs(cube)
                db_session.delete(cube)
                db_session.commit()
                raise
        else:
            remove_balances(ex_id, cube)
            remove_all_txs(cube)
            db_session.delete(cube)
            db_session.commit()
            message = 'There was a problem adding your API keys.'
            return message         
    else:
        remove_balances(ex_id, cube)
        db_session.delete(cube)
        db_session.commit()
        message = 'There was a problem adding your API keys.'
        return message


def update_key(cube, ex_id, key, secret, passphrase):
    remove_balances(ex_id, cube)
    Connection.query.filter_by(
            cube_id=cube.id,
            exchange_id=ex_id).update({'key': e(key),
                                       'secret': e(secret),
                                       'passphrase': e(passphrase),
                                       'failed_at': None
                                       })
    # Add initial transactions to database
    if add_balances(ex_id, key, secret, passphrase, cube):
        # Generate cube daily performance and update account performance
        message = 'API keys were updated successfully. Exchange connection is live!'
        cube.log_user_action("save_api_keys")
        cube.update_charts = 1
        cube.save_to_db()
        return message
    else:
        remove_balances(ex_id, cube)
        message = 'There was a problem updating your API keys.'
        abort(500, message=message)


def remove_key(cube, ex_id):
    app.logger.debug('[%s] Removing keys for Ex_ID: %s' % (cube, ex_id))
    try:
        # Add withdraw tx and remove balances
        add_txs(ex_id, cube, ttype='withdraw')
        remove_balances(ex_id, cube)
        Order.query.filter_by(cube_id=cube.id).filter(Order.ex_pair.has(exchange_id=ex_id)).delete(synchronize_session='fetch')
        # Delete Connection
        Connection.query.filter_by(
                cube_id=cube.id,
                exchange_id=ex_id).delete()
        app.logger.info('[%s] Removed keys for Ex_ID: %s' % (cube, ex_id))
        # Generate cube daily performance and update account performance
        update_performance_charts(cube)
        message = 'API connection successfully removed.'
        cube.log_user_action("save_api_keys")
        cube.update_charts = 1
        cube.save_to_db()
        return message
    except:
        message = 'API connection was not removed.'
        abort(500, message=message)


def get_balances(ex_id, key, secret, passphrase, cube):
    ex_name = Exchange.query.filter_by(id=ex_id).first().name
    try:
        if ex_name in ['External', 'Manual']:
            bals = get_all_external_balances(cube)
        else:
            bals = get_request('/balances', ex_name, key, secret, passphrase)
        app.logger.debug(bals)
        return bals
    except Exception as e:
        app.logger.debug('[%s] API keys do not work: %s' % (cube, ex_id))
        app.logger.debug(e)
        return None


def add_balances(ex_id, key, secret, passphrase, cube):
    # Find balances and add to balances table
    bals = get_balances(ex_id, key, secret, passphrase, cube)
    ex = Exchange.query.filter_by(id=ex_id).first()
    app.logger.debug("[%s] Adding balances for Ex_ID: %s" % (cube, ex_id))
    if not bals:
        return False
    try:
        # Handle External/Manual balances
        if ex.name in ['External', 'Manual']:
            for sym in bals.keys():
                cur = Currency.query.filter_by(symbol=sym).first()
                if not cur:
                    continue               
                bal = Balance(
                    cube=cube,
                    exchange_id=ex.id,
                    currency_id=cur.id,
                    available=bals[sym]['available'],
                    total=bals[sym]['total'],
                    last=bals[sym]['total']
                    )
                db_session.add(bal)
            db_session.commit()
            return True
        # Handle exchanges
        virgins = ex.name not in ['Coinbase Pro', 'Poloniex']
        # Coinbase Pro only includes tradeable currencies.
        # Poloniex returns all virgin currencies.
        eps = ExPair.query.filter_by(
            exchange=ex,
            active=True
            ).all()
        all_curs = {}
        for ep in eps:
            all_curs[ep.quote_currency] = ep.quote_symbol
            all_curs[ep.base_currency] = ep.base_symbol
        for cur in all_curs:
            sym = all_curs[cur]
            if sym in bals:
                avail = bals[sym]['available']
                total = bals[sym]['total']
            elif virgins:
                avail = 0
                total = 0
            else:
                continue
            bal = Balance(
                cube=cube,
                exchange_id=ex.id,
                currency_id=cur.id,
                available=avail,
                total=total,
                last=total
                )
            db_session.add(bal)
        db_session.commit()
        return True

    except Exception as e:
        app.logger.debug("[%s] Problem, adding balances for Ex_ID: %s" % (cube, ex_id))
        app.logger.debug(e)
        return False


def remove_balances(ex_id, cube):
    app.logger.debug("[%s] Removing balances for Ex_ID: %s" % (cube, ex_id))
    Balance.query.filter_by(exchange_id=ex_id, cube_id=cube.id).delete()
    db_session.commit()


def remove_all_txs(cube):
    app.logger.debug("[%s] Removing all transactions" % (cube))
    Transaction.query.filter_by(cube_id=cube.id).delete()
    db_session.commit()


def add_txs(ex_id, cube, ttype='deposit'):
    app.logger.debug("[%s] Adding transactions for Ex_ID: %s" % (cube, ex_id))
    try:
        # Grab recorded balances and add as initial transactions
        balances = [bal for bal in Balance.query.filter_by(
                                        cube_id=cube.id,
                                        exchange_id=ex_id).all()]
        cur_ids = [bal.currency_id for bal in balances]

        new_bals = {}
        for bal in balances:
            new_bals[bal.currency_id] = bal

        for cur_id in list(new_bals.keys()):
            if cur_id not in new_bals:
                # new balance already included in another tx
                continue
            # find suitable ex_pair
            # WARNING: assumes conjugate balance available (even if 0)
            ex_pair = ExPair.query.filter_by(
                    exchange_id=ex_id,
                    active=True
                ).filter(or_(and_(
                        ExPair.quote_currency_id == cur_id,
                        ExPair.base_currency_id.in_(cur_ids)
                    ), and_(
                        ExPair.base_currency_id == cur_id,
                        ExPair.quote_currency_id.in_(cur_ids)
                ))).first()
            if not ex_pair:
                continue
            if ttype == 'deposit':
                # Draft deposit tx
                tx = Transaction(
                    cube=cube,
                    user=cube.user,
                    ex_pair=ex_pair,
                    type='deposit',
                    api_response=None,
                    exchange_rate=None
                    )
            elif ttype == 'withdraw':
                # Draft withdraw tx
                tx = Transaction(
                    cube=cube,
                    user=cube.user,
                    ex_pair=ex_pair,
                    type='withdraw',
                    quote_balance=0,
                    base_balance=0,
                    api_response=None,
                    exchange_rate=None
                    )
            else:
                # Draft deposit tx
                ## Adding a 'convert' for migration/new accounts
                # so that daily_performance() still works
                # (need multiple txs per ex_pair for code)
                offset = datetime.utcnow() - timedelta(days=1)
                tx = Transaction(
                    created_at=offset,
                    cube=cube,
                    user=cube.user,
                    ex_pair=ex_pair,
                    type='convert',
                    api_response=None,
                    exchange_rate=None
                    )
            # Needed so that the order ID is added to the transaction
            # Without order ID, first time deposits will have 0 amounts
            db_session.add(tx)
            db_session.flush()
            # Add balances for deposit and convert txs
            if ttype != 'withdraw':
                if ex_pair.quote_currency_id == cur_id:
                    tx.quote_balance = new_bals[cur_id].last
                    bal = Balance.query.filter_by(
                        cube_id=cube.id,
                        currency_id=ex_pair.base_currency_id,
                        exchange_id=ex_id
                        ).one().last
                    tx.base_balance = bal
                else:
                    tx.base_balance = new_bals[cur_id].last
                    bal = Balance.query.filter_by(
                        cube_id=cube.id,
                        currency_id=ex_pair.quote_currency_id,
                        exchange_id=ex_id
                        ).one().last
                    tx.quote_balance = bal

            db_session.add(tx)

        db_session.commit()
        app.logger.debug("[%s] Added transactions for Ex_ID: %s" % (cube, ex_id))
        return True
    except:
        app.logger.debug("[%s] Problem adding transactions for Ex_ID: %s" % (cube, ex_id))
        return False


def tx_ledger(cube):
    ledger = cube.tx_to_ledger()
    ledger = ledger[::-1]

    ledger = [list(l) for l in ledger if abs(l[4]) > 9e-8]
    for l in ledger:
        l[0] = '%s' % l[0]  # datetime to string now before jsonifying
        l[-1] = '%.8f' % l[-1]  # amount to 8 decimals
    return {'ledger': ledger}



