import os, pyqrcode, requests, json, math
from decimal import Decimal
from pprint import pprint
from flask import url_for, current_app, render_template
from flask_restful import abort
from database import Invoice, Product, ProductPrice, Ledger, UserProduct, User, ExPair, db_session
from datetime import datetime, date, timedelta
from dateutil.relativedelta import *
from .account import Account
import sys
import logging
import requests

_email_api_url = os.getenv('EMAIL_URL')
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def create_payment_address(id):
    address = ""
    #address = "38BzBZZ9jLMoXQVdQGonkNp7aafZzRV5GU" #for testing without hitting bci api
    data = {}

    tries = 0

    while len(address) != 34:
        print("Requesting payment address")
        if tries == 10:
            raise TimeoutError("address request failed 10 times in a row." + response.text)

        secret = 'aQp93pxqvSgZoi9kjJSpU'

        my_xpub = 'xpub6Bwzcm7UvCdfkxoH5jcikATWmbzfoZSTwmTuiQFcY1aZERAJUbU6GGYbS7zmLpaAr1hDiK1FFXhvgbTQtv9xAT2w5W3j5aA8nUKbTgsEeag'
        my_api_key = '8731e368-0cae-4340-840f-34a1abc44389'

        my_callback_url = 'https://coincube.develop.coincube.io/6mFbBBB7zni?secret=%s&id=%s' % (secret, str(id))

        response = requests.get("https://api.blockchain.info/v2/receive", 
              params={'callback': my_callback_url, 'key': my_api_key, 'xpub': my_xpub})

        data = response.json()

        if "address" in data:
            address = data['address']

        tries += 1

    return address


def get_open_invoice(user):
    return Invoice.query.filter_by(
        user_id = user.id,
        paid_at = None
    ).order_by(Invoice.created_at.desc()).first()


def check_months(months):
    # correct months if value not 1, 6 or 12
    if months < 1 or (months > 1 and months < 6):
        months = 1
    if months > 6 and months < 12:
        months = 6
    if months > 12:
        months = 12

    return months

def get_product_price(type, months):
    product = Product.query.filter_by(full_name=type).first()
    return ProductPrice.query.filter_by(
            product=product,
            enabled=True,
            months=months
        ).first()

def get_pro_upgrade_price(user):
        user_product = UserProduct.query.filter_by(
            user_id=user.id
        ).first()

        if not user_product or user_product.product_type != "basic":
            return False

        inv = user_product.get_last_invoice()
        if not inv:
            return False

        pp_basic = ProductPrice.query.filter_by(
            product_type = "basic",
            months = inv.months
            ).first()

        pp_pro = ProductPrice.query.filter_by(
            product_type = "pro",
            months = inv.months
            ).first()

        price_diff = (pp_pro.monthly_rate - pp_basic.monthly_rate) * inv.months

        paid_until = user_product.paid_until

        month = paid_until.month - inv.months
        year = paid_until.year

        if month < 1:
            month = 12 + month 
            year = year - 1

        paid_from = datetime(year, month, paid_until.day)

        diff = paid_until - paid_from
        plan_days = diff.days
        diff = paid_until - datetime.now()
        days_left = diff.days
        pro_rate = Decimal(days_left / plan_days)

        return "{0:.2f}".format(price_diff * pro_rate)


def get_btc_fiat_rate(quote_symbol):
    # Get last price for BTC/USD on Bitstamp
    ex_pair = ExPair.query.filter_by(exchange_id=4, 
                                     quote_symbol=quote_symbol, 
                                     base_symbol="BTC").first()
    return ex_pair.get_close()


def coinpayments_ipn(data):
    log.debug("CP PAYMENT RECEIVED")

    if not data:
        return None

    invoice = Invoice.query.filter(  
            Invoice.id == int(data['invoice'])
        ).first()

    if invoice:
        amount_paid = data['amount1']
        if data['currency1'] != "USD":
            abort(400, message='Original currency mismatch!')

        if float(amount_paid) < float(invoice.amount):
            abort(400, message='Amount is less than order total!')

        if Ledger.query.filter(
                Ledger.tx_hash == data["txn_id"]
            ).count(): 
            abort(400, message='Invoice already paid!')

        if not int(data['status']) < 1:
            abort(400, message='Payment still pending')

        user_id = invoice.user_id

        payment = Ledger(
            user_id = user_id,
            wallet_provider = "coinpayments",
            tx_hash = data["txn_id"],
            amount = float(amount_paid),
            currency = "USD",
            type = "payment",
            invoice_id = invoice.id,
            crypto_amount = float(data["amount2"]),
            crypto_currency = data["currency2"]
        )

        db_session.add(payment)

        # create new UserProduct or update existing for Pro users
        user_product = UserProduct.query.filter_by(
                user_id=user_id
            ).first()

        if invoice.product_type == "pro_upgrade":
            product_type = "pro"
        else:
            product_type = invoice.product_type

        #calculate paid until date from months paid
        if user_product:
            user_product.product_type = product_type
        else:
            user_product = UserProduct(
                    user_id= user_id,
                    product_type=product_type
                )
            db_session.add(user_product)
            db_session.commit()
            db_session.refresh(user_product)

        if user_product.paid_until and user_product.paid_until > datetime.now():
            pay_from = user_product.paid_until
        else:
            pay_from = datetime.now()

        #update user_product record with paid_until
        user_product.paid_until = pay_from + relativedelta(months=invoice.months)
        log.debug(f'User Product paid until: {user_product.paid_until}')

        # update invoice record
        if invoice.amount_paid:
            invoice.amount_paid = float(invoice.amount_paid) + float(amount_paid)
        else:
            invoice.amount_paid = float(amount_paid)

        invoice.paid_at = datetime.now()
        invoice.user_product_id = user_product.id
        
        db_session.commit()
        #forces paid_at to load as datetime object and not string in jinja2 template
        db_session.refresh(invoice)

        log.debug(f'Invoice details: {invoice}')
        # send payment received email to customer
        data = {}
        data['paid_at'] = int(invoice.paid_at.timestamp())
        data['amount_paid'] = str(invoice.amount_paid)
        data['product'] = invoice.product.full_name
        data['months'] = invoice.months
        data['paid_until'] = int(user_product.paid_until.timestamp())
        data['method'] = payment.crypto_currency
        data['user_id'] = user_id
        data['username'] = invoice.user.first_name
        data['email'] = invoice.user.email
        data['subject'] = "We have received your payment."

        url = _email_api_url + '/user_notification/payment_received'
        r = requests.post(url, data=data)
        if r.status_code == 200:
            log.debug(f'{invoice.user}: Payment received email sent.')
            return {'message': 'success'}
        else:
            log.debug(r.status_code)
            error = f'Error with status code {r.status_code}'
            return {'message': error}

    else:
        return abort(400, message='Transaction not recognized!')



def blockchain_callback(data):
    if data['secret'] == "aQp93pxqvSgZoi9kjJSpU":
        sys.stdout.write("BTC TRANSACTION RECEIVED: " + datetime.utcnow().ctime() + str(data))

        # for (key, val) in request.values.items():
        #     sys.stdout.write(log_tag + key + ": " + val + "\n")

        invoice = Invoice.query.filter(  
                Invoice.id == data['id'],
                Invoice.address != None
            ).first()

        if invoice:
            if int(data["confirmations"]) > 2 and not Ledger.query.filter(
                    Ledger.tx_hash == data["transaction_hash"]
                ).count(): 

                user_id = invoice.user_id

                amount_paid = float(data["value"]) / 100000000 

                payment = Ledger(
                    user_id = user_id,
                    wallet_provider = "blockchain",
                    payment_address = data["address"],
                    tx_hash = data["transaction_hash"],
                    amount = amount_paid,
                    currency = "BTC",
                    type = "payment",
                    invoice_id = invoice.id
                )

                db_session.add(payment)


                btc_fiat_rate = get_btc_fiat_rate("USD")

                usd_paid =  amount_paid * float(btc_fiat_rate)

                product_prices = ProductPrice.query.filter_by(product_type = "pro").\
                    order_by(ProductPrice.months.desc()).all()

                for pp in product_prices:
                    price = float(pp.months * pp.monthly_rate)
                    if usd_paid * 1.05 >= price:
                        if usd_paid <= price: # round up for payments only a little short of full price
                            months = pp.months
                        else: # give extra credit for over payment
                            months_paid = pp.months / (price / usd_paid)
                        break
                    elif pp.months == 1: # pro rate for fraction of one month paid
                        months_paid = pp.months / (price / usd_paid)

                # create new UserProduct or update existing for Pro users
                user_product = UserProduct.query.filter_by(
                        user_id=user_id,
                        product_type="pro"
                    ).first()

                #calculate paid unitl date from months paid
                if user_product:
                    pay_from = user_product.paid_until
                else:
                    pay_from = datetime.now()

                    #create user_product now since it doesn't exist yet
                    user_product = UserProduct(
                            user_id= user_id,
                            product_type="pro"
                        )
                    db_session.add(user_product)
                    db_session.commit()
                    db_session.refresh(user_product)

                #round up months paid for to calc a hypothetical full month paid until date
                paid_until_month_ceiling = pay_from + relativedelta(months=math.ceil(months_paid))
                #calc fraction of hyptothetical full months actually paid for
                fraction_paid_for = months_paid / math.ceil(months_paid)
                #calc hyptothetical full months paid for in seconds and multiply by actual faction thereof
                seconds_paid = fraction_paid_for * (
                                int(paid_until_month_ceiling.strftime('%s')) - int(pay_from.strftime('%s')))
                #add actual paid for seconds to pay_from datetime
                paid_until = pay_from + timedelta(seconds=seconds_paid)

                #udpate user_product record with paid_until
                user_product.paid_until = paid_until

                # update invoice record
                if invoice.amount_paid:
                    invoice.amount_paid = float(invoice.amount_paid) + amount_paid
                else:
                    invoice.amount_paid = amount_paid
                invoice.paid_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                invoice.user_product_id = user_product.id
                #change plan details for invoice in case they under/over paid for plan orginially selected:
                invoice.months = pp.months
                invoice.product_price_id = pp.id
                
                
                db_session.commit()
                #forces paid_at to load as datetime object and not string in jinja2 template
                db_session.refresh(invoice)

                # send payment received email to customer
                data = {}
                data['paid_at'] = int(invoice.paid_at.timestamp())
                data['amount_paid'] = str(invoice.amount_paid)
                data['product'] = invoice.product_type
                data['months'] = invoice.months
                data['paid_until'] = int(user_product.paid_until.timestamp())
                data['method'] = "Bitcoin"
                data['user_id'] = user_id
                data['username'] = invoice.user.first_name
                data['email'] = invoice.user.email
                data['subject'] = "We have received your payment."
                return str(data)

                url = _email_api_url + '/user_notification/payment_received'
                request = requests.post(url, data=data)

                # if request.status_code == 200:
                #     account.log_notification("payment_received")


                return "Thanks! " + str(request.status_code) + str(request.text) + url

            return "Thanks Again!"

        else:
            sys.stdout.write("BTC TRANSACTION ABOVE ABORTED STATUS 400" + "\n")
            return abort(400)

    else:
        return abort(400)