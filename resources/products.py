from flask_apispec import MethodResource, marshal_with, doc
from schemas import AlgorithmSchema, ProductSchema
from database import Algorithm, Product

@marshal_with(ProductSchema(many=True))
class ProductResource(MethodResource):
    @doc(tags=['Content'], description='Product info')
    def get(self):
        return Product.query.filter_by(enabled=True).order_by(Product.id).all()


@marshal_with(AlgorithmSchema(many=True))
class AlgorithmResource(MethodResource):
    @doc(tags=['Content'], description='Mode info')
    def get(self):
        return Algorithm.query.filter_by(active=True).order_by(Algorithm.created_at).all()