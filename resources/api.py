import os
from decimal import Decimal as dec
import pandas as pd
import requests
from sqlalchemy import and_, func
from flask_apispec import MethodResource, doc, marshal_with, use_kwargs as use_kwargs_doc
from flask_restful import abort
from flask import Response, jsonify
from webargs import fields, validate
from webargs.flaskparser import use_kwargs
from flask_bcrypt import check_password_hash
from schemas import AssetSchema, CubeLimitedSchema
from database import (AssetAllocation, CombinedExPair1h, CombinedIndexPair1h, Cube, Currency, 
                      db_session, Exchange, ExPair, Focus, IndexPair, User, UserApiKey)
from .tools.cube import get_balance_data, asset_allocations_from_balances


_exapi_url = os.getenv('EXAPI_URL')

# ----------- API Resources

auth_args = {
    'key': fields.Str(required=True, description='API key'),
    'secret': fields.Str(required=True, description='API secret'),
}

def data_frame(query, columns):
    # Takes a sqlalchemy query and a list of columns, returns a dataframe.
    def make_row(x):
        return dict([(c, getattr(x, c)) for c in columns])
    return pd.DataFrame([make_row(x) for x in query])

def verify_credentials(key, secret):
    if not key or not secret:
        abort(401, message='Missing key or secret.')
    api_key = UserApiKey.query.filter_by(key=key).first()
    if not api_key or not check_password_hash(api_key.secret, secret):
        abort(401, message='Incorrect key or secret.')
    return User.query.filter_by(id=api_key.user_id).first()

def verify_credentials_developer(key, secret):
    if not key or not secret:
        abort(401, message='Missing key or secret.')
    api_key = UserApiKey.query.filter_by(key=key).first()
    if not api_key or not check_password_hash(api_key.secret, secret):
        abort(401, message='Incorrect key or secret.')
    user = User.query.filter_by(id=api_key.user_id).first()
    # Check for user with Developer role
    if not is_developer(user):
        abort(401, message='You do not have developer permissions.')
    return user

def is_developer(user):
    # Check for user with Developer role
    if user.roles.first().name == 'Developer':
        return True
    else:
        return False


class ApiSummary(MethodResource):
    @use_kwargs(auth_args, locations=('json', 'form'))
    @use_kwargs_doc(auth_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves account summary info.')
    def post(self, key, secret):
        user = verify_credentials(key, secret)
        try:
            cubes = Cube.query.filter_by(
                            user_id=user.id
                            ).filter(and_(
                            Cube.closed_at == None,
                            func.length(Cube.balances) > 0,
                            )).all()
            if cubes:
                cube_ids = [cube.id for cube in cubes]
                balances, total, performance_fiat = get_balance_data(cubes, user)
                allocations = asset_allocations_from_balances(balances)
                return {
                    'balances': balances, 
                    'allocations': allocations,
                    'total': total,
                    'cubes': cube_ids
                    }
            else:
                return {}
        except:
            abort(500) 

class ApiCubeSummary(MethodResource):
    post_args = {**auth_args, **{
        'cube_id': fields.Int(required=True, description='Cube ID'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves cube summary info.')
    def post(self, key, secret, cube_id):
        user = verify_credentials(key, secret)
        try:
            cube = Cube.query.filter_by(
                            user_id=user.id,
                            id=cube_id,
                            ).first()
            if cube:
                balances, total, performance_fiat = get_balance_data([cube], user)
                allocations = asset_allocations_from_balances(balances)
                return {
                    'balances': balances, 
                    'allocations': allocations,
                    'total': total,
                    }
            else:
                return {}
        except:
            abort(500) 


@marshal_with(CubeLimitedSchema())
class ApiCubeDetails(MethodResource):
    post_args = {**auth_args, **{
        'cube_id': fields.Int(required=True, description='Cube ID'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves cube open orders')
    def post(self, key, secret, cube_id):
        user = verify_credentials(key, secret)
        cube = Cube.query.get(cube_id)
        if cube:
            return cube
        else:
            message = 'No cube associated with ID {}'.format(cube_id)
            abort(404, message=message)


class ApiCandles(MethodResource):
    post_args = {**auth_args, **{
        'base': fields.Str(required=True, description='Base symbol'),
        'quote': fields.Str(required=True, description='Quote symbol'),
        'timestamp': fields.Str(required=True, description='Timestamp'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves candles')
    def post(self, key, secret, base, quote, timestamp):
        user = verify_credentials_developer(key, secret)

        base = Currency.query.filter_by(symbol=base).first()
        quote = Currency.query.filter_by(symbol=quote).first()
        # Try for index pair
        try:
            index_pair = IndexPair.query.filter_by(
                                        base_currency_id=base.id,
                                        quote_currency_id=quote.id,
                                        ).first()
        except:
            abort(404, message='Invalid base/quote/exchange.')

        query = index_pair.candle_1h_query
        if timestamp:
            ep_table = CombinedIndexPair1h
            query =  query.filter(
                        ep_table.timestamp >= timestamp
                        )
        candles = pd.read_sql(query.statement, query.session.bind,
            index_col='timestamp')
        candles = candles[['close', 'high', 'low', 'volume', 'open']]
        return candles.to_json()


def add_pair(pairs, pair, timestamp):
    print("Adding pair ", pair.base_symbol, "/", pair.quote_symbol)
    ep_table = CombinedIndexPair1h
    query = pair.candle_1h_query.filter(
                ep_table.timestamp >= timestamp
                )
    candle = pd.read_sql(query.statement, query.session.bind,
        index_col='timestamp')
    if pair.quote_symbol == 'USD':
        pairs[pair.base_symbol] = candle.close
    else:
        pairs[pair.base_symbol] = candle.close * pairs['BTC']
    return True

def add_all_pairs(index_pairs, timestamp):
    pairs = pd.DataFrame()

    for pair in index_pairs:
        if add_pair(pairs, pair, timestamp):
            pairs[pair.base_symbol] = pairs[pair.base_symbol].astype(float)
            pairs[pair.base_symbol] = pairs[pair.base_symbol].fillna(method='ffill')
            pairs[pair.base_symbol] = pairs[pair.base_symbol].fillna(0)
    pairs['timestamp'] = pairs.index
    pairs['timestamp'] = pairs.timestamp.astype(str)
    return pairs.to_json()

class ApiClose1h(MethodResource):
    post_args = {**auth_args, **{
        'timestamp': fields.Str(required=True, description='Timestamp'),
        'index': fields.Str(required=False, description='Index')
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves close in USD terms')
    def post(self, key, secret, timestamp, index='top_ten'):
        user = verify_credentials_developer(key, secret)

        focus = Focus.query.filter_by(type=index).first()
        symbols = []
        for cur in focus.currencies:
            symbols.append(cur.symbol)
        # Index Pairs
        index_pairs = IndexPair.query.filter_by(active=True
                                ).filter(and_(
                                    IndexPair.quote_symbol.in_(['BTC', 'USD']),
                                    IndexPair.base_symbol.in_(symbols),
                                    )).all()

        return add_all_pairs(index_pairs, timestamp)


def get_price(exchange, base, quote):
    url = f'{_exapi_url}/{exchange}/midprice'
    params = {
        'base': base,
        'quote': quote
    }
    r = requests.get(url, params=params)
    if r.status_code == 200:
        price = r.json()
        return dec(price['price_str'])
    else:
        return None

class ApiPriceCacher(MethodResource):
    post_args = {**auth_args, **{
        'exchange': fields.Str(required=True, description='Exchange'),
        'base': fields.Str(required=True, description='Base'),
        'quote': fields.Str(required=True, description='Quote'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves close in USD terms')
    def post(self, key, secret, exchange, base, quote):
        user = verify_credentials_developer(key, secret)
        return get_price(exchange, base, quote)


class ApiPortfolios(MethodResource):
    post_args = {**auth_args, **{
        'algorithm_id': fields.Int(required=True, description='Algorithm ID'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Retrieves cube portfolio')
    def post(self, key, secret, algorithm_id):
        user = verify_credentials_developer(key, secret)

        cubes = Cube.query.filter_by(algorithm_id=algorithm_id).all()
        if not cubes:
            abort(404, message='There are no cubes flagged with this algorithm.')     
        else:
            portfolios = {}
            for cube in cubes:
                # Add index portfolio assets if selected
                if cube.focus_id:
                    curs = cube.focus.currencies
                else:
                    custom = CustomPortfolio.query.filter_by(cube_id=cube.id, selected=True).first()
                    curs = custom.currencies
                assets = []
                for cur in curs:
                    assets.append(cur.symbol)
                portfolios[cube.id] = assets

        return jsonify(portfolios)


class ApiPostAllocations(MethodResource):
    post_args = {**auth_args, **{
        'algorithm_id': fields.Int(required=True, description='Algorithm ID'),
        'allocations': fields.Nested(AssetSchema, required=True, many=True),
        'index': fields.Str(required=False, description='Index'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['API'], description='Post allocations for Optimized Index')
    def post(self, key, secret, algorithm_id, allocations, index='top_ten'):
        user = verify_credentials_developer(key, secret)

        if algorithm_id != 6:
            message = 'Currently only allowing allocation adjustments for Risk Optimized Cubes.'
            abort(404, message=message)
        cubes = Cube.query.filter_by(algorithm_id=algorithm_id).all()

        for cube in cubes:
            if cube:
                new_list = []
                for allocation in allocations:
                    new_list.append(allocation['asset'])
                    currency = Currency.query.filter_by(symbol=allocation['asset']).first()
                    a = AssetAllocation.query.filter_by(cube_id=cube.id, currency=currency).first()

                    if a: # update existing allocation
                        a.percent = allocation['percent']
                        a.save_to_db()
                    else: # save new allocation 
                        new_allocation = AssetAllocation(
                                        cube_id = cube.id,
                                        currency = currency,
                                        percent = allocation['percent']
                                    )

                        new_allocation.save_to_db()
                # Zero out other non-allocated assets
                for symbol in cube.allocations:
                    if symbol not in new_list:
                        cube.allocations[symbol].percent = 0
            db_session.add(cube)
        db_session.commit()
        return 'success'
