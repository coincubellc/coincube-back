import requests
from requests_toolbelt.utils import dump
import json
import pandas as pd
from datetime import datetime


###########################################################
######### Fetching Newest Market Data #####################


_baseurl = 'https://coincube.io/api/v1/'
auth_args = {'key': 'c11abd3ab60bc39192ef70cf8c24015', 'secret': '3120c2064d1cfbe2086bb4ce664e76'}
# _baseurl = 'https://coincube-back.develop.coincube.io/api/v1/'
# auth_args = {'key': '9668b10ee6c070c020be9d8b2ff1191', 'secret': '7ae9b8d1811f14eea256d066604d221'}

def get_candles(base, quote, timestamp='2018-1-1'):
    url = _baseurl + 'candles'
    data = {**auth_args, **{
        'base': base, 
        'quote': quote, 
        'timestamp': timestamp
    }}
    r = requests.post(url, data=data) 
    if r.status_code == 200:
        df = pd.read_json(r.json())
        return df
    else:
        return "There was a problem: " + str(r.status_code)

def close_1h(index='top_ten', timestamp='2018-1-1'):
    url = _baseurl + 'close_1h'
    data = {**auth_args, **{
        'index': index,
        'timestamp': timestamp
    }}
    # POST request
    r = requests.post(url, data=data)
    df = pd.read_json(r.json())
    # Dump DataFrame to CSV
    file_name = '1h_close.csv'
    df.to_csv(file_name)
    if r.status_code == 200:
        return "Import of Price data completed"
    else:
        return "There was a problem: " + str(r.status_code)

def cube_details(cube_id):
    url = _baseurl + 'cube_details'
    data = {**auth_args, **{
        'cube_id': cube_id
    }}
    # POST request
    r = requests.post(url, data=data)
    print(r)
    print(r.json())
    # Return JSON data
    content = r.json()
    if r.status_code == 200:
        return content
    else:
        return "There was a problem: " + str(r.status_code)

def get_portfolios(algorithm_id=6):
    url = _baseurl + 'get_portfolios'
    data = {**auth_args, **{
        'algorithm_id': algorithm_id
    }}
    # POST request
    r = requests.post(url, data=data)
    # Return JSON data
    content = r.json()
    if r.status_code == 200:
        return content
    else:
        return "There was a problem: " + str(r.status_code)

def post_allocations(allocations, algorithm_id=6):
    url = _baseurl + 'post_allocations'
    data = {**auth_args, **{
        'allocations': allocations,
        'algorithm_id': algorithm_id
    }}
    # POST request
    r = requests.post(url, json=data)
    data = dump.dump_all(r)
    # Return 'success' or status code
    if r.status_code == 200:
        return "success"
    else:
        return "There was a problem: " + str(r.status_code)

def price_cacher(exchange, base, quote):
    url = _baseurl + 'price_cacher'
    data = {**auth_args, **{
        'exchange': exchange,
        'base': base,
        'quote': quote
    }}
    # POST request
    r = requests.post(url, data=data)
    # Return JSON data
    content = r.json()
    if r.status_code == 200:
        return content
    else:
        return "There was a problem: " + str(r.status_code)

