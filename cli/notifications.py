import sys, time, hashlib, os, requests
from flask import Flask, current_app
from datetime import datetime, timedelta
from calendar import monthrange
from dateutil.relativedelta import relativedelta

sys.path.append('/coincube-back/')
from resources.tools.account import Account
from database.database import (extract, db_session, User, Cube, 
                        UserNotification, UserProduct, 
                        Connection, Algorithm, Transaction, ExPair)

_email_api_url = os.getenv('EMAIL_URL')



#@app.cli.command()
def notify_users():
    while True:
        url = _email_api_url + '/user_notification/exception'

        try:
            now = datetime.utcnow()
            start = now - timedelta(days = 7)
            end = now - timedelta(hours = 1)

            recent_cubes = Cube.query.filter(
                Cube.created_at >= start.strftime('%Y-%m-%d %H:%M:%S'), 
                Cube.created_at <= end.strftime('%Y-%m-%d %H:%M:%S')
            ).all()

            notify_cube_opened(recent_cubes)
        except: 
            error = str(sys.exc_info())
            print("*** EXCEPTION in notify_cube_opened: " + error)
            data = {"type": "notify_cube_opened", "error": error}
            request = requests.post(url, data=data)



        try:
            now = datetime.utcnow()
            start = now - timedelta(days = 8)
            end = now - timedelta(hours = 24)

            recent_cubes = Cube.query.filter(
                            Cube.created_at >= start.strftime('%Y-%m-%d %H:%M:%S'), 
                            Cube.created_at <= end.strftime('%Y-%m-%d %H:%M:%S'),
                            Cube.suspended_at == None,
                            Cube.closed_at == None
                        ).all()


            notify_finish_setup(recent_cubes)

        except: 
            error = str(sys.exc_info())
            print("*** EXCEPTION in notify_finish_setup: " + error)
            data = {"type": "notify_finish_setup", "error": error}
            request = requests.post(url, data=data)

        try:
            now = datetime.utcnow()
            start = now - timedelta(days = 200)
            end = now - timedelta(days = 2)

            closed_cubes = Cube.query.filter(
                                Cube.created_at >= start.strftime('%Y-%m-%d %H:%M:%S'), 
                                Cube.created_at <= end.strftime('%Y-%m-%d %H:%M:%S'),
                                Cube.closed_at != None
                            ).order_by(Cube.closed_at.desc()).group_by(Cube.user_id).all()

            send_exit_survey(closed_cubes);

        except: 
            error = str(sys.exc_info())
            print("*** EXCEPTION in send_exit_survey: " + error)
            data = {"type": "send_exit_survey", "error": error}
            request = requests.post(url, data=data)

        try:
            now = datetime.utcnow()

            # check cubes once per day by filtering for created_at current hour
            open_cubes = Cube.query.filter(
                                #extract('weekday', Cube.created_at) == now.weekday(),
                                extract('hour', Cube.created_at) == now.hour, 
                                Cube.suspended_at == None,
                                Cube.closed_at == None
                            ).group_by(Cube.user_id).all()

            notify_cube_below_min(open_cubes);

        except: 
            error = str(sys.exc_info())
            print("*** EXCEPTION in notify_cube_below_min: " + error)
            data = {"type": "notify_cube_below_min", "error": error}
            request = requests.post(url, data=data)



        try:
            failed_users = User.query.join(Cube).filter(
                            Cube.closed_at == None,
                            Cube.connections.any(Connection.failed_at != None),
                            Cube.algorithm.has(Algorithm.name.in_(['Tidalwave', 'Index']))
                        ).all()

            notify_api_failed(failed_users)

        except: 
            error = str(sys.exc_info())
            print("*** EXCEPTION in notify_api_failed: " + error)
            data = {"type": "notify_api_failed", "error": error}
            request = requests.post(url, data=data)


        try:
            in_7_days = datetime.utcnow() + timedelta(days = 7)
            expiring_products = UserProduct.query.filter(
                                    UserProduct.paid_until <= in_7_days
                            ).all()

            notify_payment_due(expiring_products)

        except:
            error = str(sys.exc_info())
            print("*** EXCEPTION in notify_payment_due: " + error)
            data = {"type": "notify_payment_due", "error": error}
            request = requests.post(url, data=data)


        time.sleep(60 * 60)


def notify_payment_due(user_products):
    date_2 = datetime.utcnow() + timedelta(days = 3)
    date_3 = datetime.utcnow() + timedelta(days = 1)

    for user_product in user_products:
        data = {}

        data['user_product'] = user_product
        product_price = user_product.get_last_invoice().product_price
        user = user_product.user

        data['paid_until'] = int(user_product.paid_until.timestamp())
        data['monthly_rate'] = product_price.monthly_rate
        data['months'] = product_price.months
        data['user_id'] = user.id
        data['username'] = user.first_name
        data['email'] = user.email
        data['subject'] = "Payment due reminder."
        

        url = _email_api_url + '/user_notification/payment_due'

        account = Account(user)
        if not account.was_notified("payment_due_1"):
            request = requests.post(url, data=data)
            if request.status_code == 200:
                account.log_notification("payment_due_1")
        elif not account.was_notified("payment_due_2") and user_product.paid_until <= date_2:
            request = requests.post(url, data=data)
            if request.status_code == 200:
                account.log_notification("payment_due_2")
        elif not account.was_notified("payment_due_3") and user_product.paid_until <= date_3:
            request = requests.post(url, data=data)
            if request.status_code == 200:
                account.log_notification("payment_due_3")


def notify_api_failed(users):
    for user in users:

        # repeat notification every 7 days until api_failed removed from notifications table
        now = datetime.utcnow()
        start = now - timedelta(days = 7)

        notification = UserNotification.query.filter(
                            UserNotification.type == "api_failed",
                            UserNotification.user_id == user.id,
                            UserNotification.created_at >= start.strftime('%Y-%m-%d %H:%M:%S'),
                        ).first()


        if not notification:
            data = {}

            # get all failed api connections
            connections = Connection.query.filter(
                            Connection.cube_id == user.cubes[-1].id,
                            Connection.failed_at != None
                        ).all()

            # find last transaction with api failure response
            for connection in connections:
                ex_pair_ids = []
                for ep in connection.exchange.ex_pairs:
                    ex_pair_ids.append(ep.id)
                transaction = Transaction.query.filter(
                            Transaction.user_id == user.id,
                            Transaction.ex_pair.has(ExPair.id.in_(ex_pair_ids)),
                            Transaction.api_response != None
                        ).order_by(Transaction.id.desc()).first()

            data['exchange_name'] = connection.exchange.name
            data['api_response'] = transaction.api_response
            data['user_id'] = user_id
            data['username'] = invoice.user.first_name
            data['email'] = invoice.user.email
            data['subject'] = "We cannot connect to your exchange API."

            url = _email_api_url + '/user_notification/api_failed'
            request = requests.post(url, data=data)

            if request.status_code == 200:
                account = Account(user)
                account.log_notification("api_failed")


def send_exit_survey(cubes):
    for cube in cubes:
        user = cube.user
        data = {}
        data['closed_at'] = int(cube.closed_at.timestamp()) 
        data['user_id'] = user.id
        data['username'] = user.first_name
        data['email'] = user.email
        data['subject'] = "Please take a moment."
        
        account = Account(user)

        if not account.was_notified("exit_survey", cube.id):
            url = _email_api_url + '/user_notification/exit_survey'
            request = requests.post(url, data=data)

            if request.status_code == 200:
                account.log_notification("exit_survey", cube.id)

def notify_cube_below_min(cubes):
    for cube in cubes:
        vals = cube.valuations()
        if vals['val_fiat'] < 200:
            user = cube.user
            data = {}
            data['val_fiat'] = vals['val_fiat']
            data['usd_min'] = 200
            data['user_id'] = user.id
            data['username'] = user.first_name
            data['email'] = user.email
            data['subject'] = "Your balance is low."
            
            account = Account(user)

            url = _email_api_url + '/user_notification/cube_below_min'
            request = requests.post(url, data=data)



def notify_cube_opened(cubes):
    for cube in cubes:
        account = Account(cube.user)
        if not account.was_notified("cube_opened", cube.id):
            user = cube.user
            data = {}
            data['user_id'] = user.id
            data['username'] = user.first_name
            data['email'] = user.email
            data['subject'] = "A Cube has been opened."

            url = _email_api_url + '/user_notification/cube_opened'
            request = requests.post(url, data=data)

            if request.status_code == 200:
                account.log_notification("cube_opened", cube.id)

def notify_finish_setup(cubes):
    for cube in cubes:
        #print("Cube id: %s connections: %s, allocations: %s" % (cube.id, len(cube.connections), len(cube.allocations)))

        if len(cube.connections) > 0 and len(cube.allocations) > 0:
            continue

        user = cube.user
        account = Account(user)
        data = {}
        data['user_id'] = user.id
        data['username'] = user.first_name
        data['email'] = user.email

        if len(cube.connections) == 0:
            if not account.was_notified("no_api_connection", cube.id):
                data['subject'] = "You need to setup an api connection"
                url = _email_api_url + '/user_notification/no_api_connection'
                request = requests.post(url, data=data)

                if request.status_code == 200:
                    account.log_notification("no_api_connection", cube.id)

        elif len(cube.allocations) == 0 and not account.was_notified("no_allocations", cube.id):
            data['subject'] = "You need to setup your portfolio"
            url = _email_api_url + '/user_notification/no_allocations'
            request = requests.post(url, data=data)

            if request.status_code == 200:
                account.log_notification("no_allocations", cube.id)   


notify_users()