#!/usr/bin/env python3
import os
from flask_restful import Api, Resource, abort
from flask_apispec import FlaskApiSpec
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from webargs.flaskparser import parser
from resources import *
from database import app, db_session, init_db, RevokedToken


# Enable Cross Origin Resource Sharing for all domains on all routes
CORS(app)
# Add JSON Web Token authorization
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedToken.is_jti_blacklisted(jti)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.after_request
def after_request(response):
    """Close the database connection after each request."""
    db_session.commit()
    return response

# Flask RESTful API
api = Api(app)
docs = FlaskApiSpec(app)

resources = {
    # Account resources
    '/api/account/api': ApiKey,
    '/api/account/available_algorithms': AvailableAlgorithms,
    '/api/account/available_exchanges': AvailableExchanges,
    '/api/account/balances': AccountBalances,
    '/api/account/invoices': InvoiceResource,
    '/api/account/payments': PaymentResource,
    '/api/account/payment/coinpayments': CoinPaymentsPayment,
    '/api/account/payment/pro_upgrade': ProUpgradePayment,
    '/api/account/pro_upgrade_cost': ProUpgradeCost,
    '/api/account/performance/<string:timeframe>': PerformanceAccountResource,
    '/api/account/payment/status': PaymentStatus,
    '/api/account/save_email': SaveEmail,
    '/api/account/save_password': SavePassword,
    '/api/account/save_second_factor': SaveSecondFactor,
    '/api/account/second_factor_secret': SecondFactorSecret,
    '/api/account/save_setting': SaveUserSetting,
    '/api/account/send_support_email': SendSupportEmail,
    '/api/account/user': UserResource,
    '/api/account/valuations': AccountValuations,

    # Admin resources
    '/api/admin/billing_stats': BillingStats,
    '/api/admin/cube_stats': CubeStats,
    '/api/admin/login_as_user': LoginAsUser,
    '/api/admin/paid_invoices': AdminPaidInvoices,
    '/api/admin/payments': AdminPayments,
    '/api/admin/unpaid_invoices': AdminUnpaidInvoices,
    '/api/admin/users': Users,
    '/api/admin/user_stats': UserStats,

    # Coincube API resources
    '/api/v1/summary': ApiSummary,
    '/api/v1/cube/summary': ApiCubeSummary,
    '/api/v1/cube/details': ApiCubeDetails,
    '/api/v1/candles': ApiCandles,
    '/api/v1/close_1h': ApiClose1h,
    '/api/v1/get_portfolios': ApiPortfolios,
    '/api/v1/post_allocations': ApiPostAllocations,
    '/api/v1/price_cacher': ApiPriceCacher,

    # Authorization resources
    '/api/auth/confirm/<string:token>': ConfirmEmail,
    '/api/auth/login':  Login,
    '/api/auth/logout_access': LogoutAccess,
    '/api/auth/logout_refresh': LogoutRefresh,   
    '/api/auth/validate_oauth/': OauthValidate,
    '/api/auth/register': Register,
    '/api/auth/refresh': TokenRefresh,
    '/api/auth/reset_password_token': ResetPasswordToken,
    '/api/auth/reset_password/<string:token>': ResetPassword,
    '/api/auth/second_factor': SecondFactor,

    # Front section API resources
    '/api/chart/performance/<string:index_type>/<string:index_name>': PerformanceChart,
    '/api/chart/performance/<string:index_type>': PerformanceCharts,
    '/api/chart/pie/<string:index_type>/<string:index_name>': PieChart,
    '/api/chart/pie/<string:index_type>': PieCharts,
    '/api/charts/pie': AllPieCharts,
    '/api/cmc/id': CmcId,
    '/api/cmc/ids': CmcIds,
    '/api/indices': Indices,
    '/api/instructions/<string:key>': InstructionResource,
    '/api/faq': FAQResource,
    '/api/modes': AlgorithmResource,
    '/api/products': ProductResource,
    '/api/supported_assets': SupportedAssets,
    '/api/supported_exchanges': SupportedExchanges,
    '/api/supported_exchange_assets': SupportedExchangeAssets,
    '/api/supported_exchange_pairs': SupportedExchangePairs,
    '/api/team': TeamResource,

    # 3rd Party Callbacks
    '/callback/cp/ipn': CoinPaymentsIPN,

    # Cube API resources
    '/api/cube': CubeResource,
    '/api/cube/allocations/current': AllocationsCurrent,
    '/api/cube/allocations/target': AllocationsTarget,
    '/api/cube/available_ex_assets': AvailableAssets,
    '/api/cube/balances': Balances,
    '/api/cube/connection': ConnectionResource,
    '/api/cube/cost_average': CostAverage,
    '/api/cube/cost_average_items': CostAverageItems,
    '/api/cube/ex_pairs': ExPairResource,
    '/api/cube/performance/<string:timeframe>': PerformanceResource,
    '/api/cube/external/balances': ExternalBalances,
    '/api/cube/save_setting': SaveCubeSetting,
    '/api/cube/transactions': Transactions,
    '/api/cube/transactions_full': TransactionsFull,
    '/api/cube/valuations': Valuations,

    # Healthcheck
    '/health': Healthcheck,

    # Wallet API resources
    '/api/wallet': WalletResource,
    '/api/wallet/new': AddWallet,
    '/api/wallet/allocations': WalletAllocationsCurrent,
    '/api/wallet/available_ex_assets': WalletAvailableAssets,
    '/api/wallet/balances': WalletBalances,
    '/api/wallet/performance/<string:timeframe>': WalletPerformanceResource,
    '/api/wallet/external/address': WalletExternalAddressResource,
    '/api/wallet/external/blockchains': WalletExternalBlockchains,
    '/api/wallet/external/refresh_balances': WalletRefreshExternalBalances,
    '/api/wallet/save_setting': SaveWalletSetting,
    '/api/wallet/transactions': WalletTransactions,
    '/api/wallet/valuations': WalletValuations,

}

for key, value in resources.items():
    # Add API resources
    api.add_resource(value, key)
    # Register documentation
    docs.register(value)

# Build the database:
# This will create the database file using SQLAlchemy
try:
    init_db()
    print('DB INITALIZED')
except:
    app.logger.exception('Empty database. Unable to run init_db().')

# This error handler is necessary for webargs usage with Flask-RESTful.
@parser.error_handler
def handle_request_parsing_error(err, req):
    abort(422, errors=err.messages)

if __name__ == '__main__':
    PORT = int(os.getenv('PORT'))
    HOST = os.getenv('HOST')
    print ("Starting server..")

