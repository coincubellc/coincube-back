from database import db_session, Invoice, User, UserProduct
from datetime import datetime, timedelta

# Get all users
users = User.query.all()

for user in users:
    # Fix user stuff
    user.fiat_id = 1
    user.btc_data = 0
    user.wide_charts = 0
    db_session.add(user)
    # Create User UserProduct for each
    user_product = UserProduct(
                user_id=user.id,
                product_type='pro',
                paid_until=datetime.utcnow() + timedelta(days=30),
                subscribed=True,
            )
    db_session.add(user_product)
    db_session.commit()
    db_session.refresh(user_product)

    invoice = Invoice(
                user_id=user.id,
                product_type='pro',
                amount=15.0,
                currency='USD',
                months=1,
                paid_at=datetime.utcnow(),
                amount_paid=0,
                product_price_id=1,
                user_product_id=user_product.id,
            )
    db_session.add(invoice)
    db_session.commit()