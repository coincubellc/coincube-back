from marshmallow import Schema, fields


class AlgorithmSchema(Schema):
    active = fields.Bool()
    name = fields.Str()
    features = fields.List(fields.Str())


class AssetSchema(Schema):
    asset = fields.Str()
    percent = fields.Decimal()


class CurrencySchema(Schema):
    symbol = fields.Str()
    name = fields.Str()
    market_cap = fields.Int()
    cmc_id = fields.Str()
    percent_change_24h = fields.Decimal()
    percent_change_7d = fields.Decimal()


class DailyPerformanceSchema(Schema):
    timestamp = fields.DateTime()
    btc_total = fields.Decimal()
    start_val = fields.Decimal()
    end_val = fields.Decimal()
    daily_return = fields.Decimal()


class CostAverageSchema(Schema):
    id = fields.Int()
    cube_id = fields.Int()
    base_currency_id = fields.Int()
    quote_currency_id = fields.Int()
    side = fields.Str()
    value = fields.Decimal()
    frequency = fields.Int()
    base_currency = fields.Nested(CurrencySchema)
    quote_currency = fields.Nested(CurrencySchema)


class ExchangeSchema(Schema):
    name = fields.Str()
    key = fields.Str()
    secret = fields.Str()
    passphrase = fields.Str()
    video_url = fields.Str()
    signup_url = fields.Str()
    id = fields.Int()


class ExchangeAssetsSchema(Schema):
    name = fields.Str()
    signup_url = fields.Str()
    assets = fields.List(fields.Nested(CurrencySchema))
    id = fields.Int()


class ExPairLimitsSchema(Schema):
    min_amount = fields.Decimal()
    min_value = fields.Decimal()


class ExPairSchema(Schema):
    base_symbol = fields.Str()
    quote_symbol = fields.Str()
    exchange_id = fields.Int()
    ex_pair_limits = fields.List(fields.Nested(ExPairLimitsSchema))


class BalanceSchema(Schema):
    currency = fields.Nested(CurrencySchema)
    exchange = fields.Nested(ExchangeSchema)
    total = fields.Decimal()
    target = fields.Decimal()
    btc_rate = fields.Decimal()


class ExternalAddressesSchema(Schema):
    address = fields.Str()
    address_type = fields.Str()


class FocusSchema(Schema):
    type = fields.Str()
    count = fields.Int()
    currencies = fields.List(fields.Nested(CurrencySchema))


class APIConnectionsSchema(Schema):
    exchange = fields.Nested(ExchangeSchema)
    failed_at = fields.DateTime()


class CubeCacheSchema(Schema):
    processing = fields.Bool()


class OrderSchema(Schema):
    amount = fields.Decimal()
    avg_price = fields.Decimal()
    datetime = fields.Str()
    ex_pair = fields.Nested(ExPairSchema)
    order_id = fields.Str()
    side = fields.Str()
    price = fields.Decimal()
    filled = fields.Decimal()
    unfilled = fields.Decimal()
    timestamp = fields.Str()


class CubeSchema(Schema):
    algorithm = fields.Nested(AlgorithmSchema)
    api_connections = fields.List(fields.Nested(APIConnectionsSchema))
    auto_rebalance = fields.Bool()
    balanced_at = fields.DateTime()
    btc_data = fields.Bool()
    closed_at = fields.DateTime()
    cube_cache = fields.Nested(CubeCacheSchema)
    exchange = fields.Nested(ExchangeSchema)
    external_addresses = fields.List(fields.Nested(ExternalAddressesSchema))
    fiat = fields.Nested(CurrencySchema)
    fiat_id = fields.Int()
    focus = fields.Nested(FocusSchema)
    focus_id = fields.Int()
    id = fields.Int()
    is_rebalancing = fields.Bool()
    orders = fields.List(fields.Nested(OrderSchema))
    reallocated_at = fields.DateTime()
    rebalance_interval = fields.Int()
    risk_tolerance = fields.Int()
    supported_assets = fields.List(fields.Str())
    threshold = fields.Decimal()
    trading_status = fields.Str()
    unrecognized_activity = fields.Bool()
    wide_charts = fields.Bool()
    name = fields.Str()


class CubeLimitedSchema(Schema):
    algorithm = fields.Nested(AlgorithmSchema)
    api_connections = fields.List(fields.Nested(APIConnectionsSchema))
    auto_rebalance = fields.Bool()
    balanced_at = fields.DateTime()
    fiat = fields.Nested(CurrencySchema)
    is_rebalancing = fields.Bool()
    orders = fields.List(fields.Nested(OrderSchema))
    reallocated_at = fields.DateTime()
    rebalance_interval = fields.Int()
    risk_tolerance = fields.Int()
    supported_assets = fields.List(fields.Str())
    threshold = fields.Decimal()
    trading_status = fields.Str()


class FAQContentSchema(Schema):
    sub_heading = fields.Str()
    question = fields.Str()
    answer = fields.Str()


class FAQSchema(Schema):
    heading = fields.Str()
    content = fields.List(fields.Nested(FAQContentSchema))


class PieChartSchema(Schema):
    name = fields.Str()
    y = fields.Decimal()


class IndexPieChartSchema(Schema):
    index_type = fields.Str()
    chart = fields.List(fields.Nested(PieChartSchema))


class InstructionItemSchema(Schema):
    order = fields.Int()
    item = fields.Str()


class InstructionSchema(Schema):
    key = fields.Str()
    title = fields.Str()
    items = fields.List(fields.Nested(InstructionItemSchema))


class ProductPriceSchema(Schema):
    product_type = fields.Str()
    months = fields.Int()
    monthly_rate = fields.Decimal()


class ProductSchema(Schema):
    product_type = fields.Str()
    full_name = fields.Str()
    description = fields.Str()
    enabled = fields.Bool()
    prices = fields.Dict(values=fields.Decimal(), keys=fields.Str())
    features = fields.List(fields.Str())


class UserProductSchema(Schema):
    product_type = fields.Str()
    paid_until_timestamp = fields.Str()
    subscribed = fields.Bool()
    product = fields.Nested(ProductSchema)


class InvoiceSchema(Schema):
    user_id = fields.Int()
    product_type = fields.Str()
    amount = fields.Decimal()
    currency = fields.Str()
    address = fields.Str()
    months = fields.Int()
    paid_at_timestamp = fields.Str()
    product_name = fields.Str()
    amount_paid = fields.Decimal()
    product_price = fields.Nested(ProductPriceSchema)
    user_product = fields.Nested(UserProductSchema)

class PaymentSchema(Schema):
    amount = fields.Decimal()
    currency = fields.Str()
    created_at_timestamp = fields.Str()
    invoice = fields.Nested(InvoiceSchema)


class PerformanceChartSchema(Schema):
    index_type = fields.Str()
    timestamp = fields.List(fields.Float())
    performance = fields.List(fields.Decimal())


class SupportedAssetsSchema(Schema):
    header = fields.List(fields.Str())
    values = fields.List(fields.List(fields.Str()))


class TeamSchema(Schema):
    name = fields.Str()
    title = fields.Str()
    biography = fields.Str()
    github_url = fields.Str()
    linkedin_url = fields.Str()
    twitter_url = fields.Str()
    image_location = fields.Str()


class TransactionSchema(Schema):
    base_amount = fields.Decimal()
    ex_pair = fields.Nested(ExPairSchema)
    ex_pair_id = fields.Int()
    exchange_rate = fields.Decimal()
    quote_amount = fields.Decimal()
    datetime = fields.Str()
    timestamp = fields.Str()
    type = fields.Str()


class TransactionFullSchema(Schema):
    datetime = fields.Str()
    base_symbol = fields.Str()
    quote_symbol = fields.Str()
    base_amount = fields.Decimal()
    quote_amount = fields.Decimal()
    price = fields.Decimal()
    type = fields.Str()
    price = fields.Decimal()
    tx_id = fields.Str()


class UserApiKeySchema(Schema):
    key = fields.Str()


class UserNotificationSchema(Schema):
    id = fields.Int()
    type = fields.Str()
    message = fields.Str()


class UserSchema(Schema):
    api_keys = fields.List(fields.Nested(UserApiKeySchema))
    available_algorithms = fields.List(fields.Nested(AlgorithmSchema))
    available_exchanges = fields.List(fields.Nested(ExchangeSchema))
    email = fields.Email()
    first_name = fields.Str()
    email_confirmed = fields.Bool()
    otp_complete = fields.Bool()
    is_pro = fields.Bool()
    is_basic = fields.Bool()
    btc_data = fields.Bool()
    wide_charts = fields.Bool()
    portfolio = fields.Bool()
    fiat = fields.Nested(CurrencySchema)
    fiat_id = fields.Int()
    notifications = fields.List(fields.Nested(UserNotificationSchema))
    open_cubes = fields.List(fields.Nested(CubeSchema))
    open_wallets = fields.List(fields.Nested(CubeSchema))
    role = fields.Str()
    social_id = fields.Str()