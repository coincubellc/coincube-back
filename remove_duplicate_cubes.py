from resources.tools.account import delete_cube
from database import *

users = User.query.filter(User.last_login > '2018-9-14').all()

for user in users:
    if user.cubes:
        print(user)
        existing_cubes = []
        for cube in user.cubes:
            if cube.exchange_id in existing_cubes:
                print('Removing: ', cube)
                delete_cube(cube.id)
            else:
                if cube.exchange_id != 12:
                    existing_cubes.append(cube.exchange_id)
