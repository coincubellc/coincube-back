from database import Cube, ExPair, User, Transaction, db_session

cube = Cube.query.get(1)
user = User.query.get(8975)
ex_pair = ExPair.query.get(1)

tx = Transaction(
        cube=cube,
        user=user,
        ex_pair=ex_pair,
        type='deposit'
        )

db_session.add(tx)
db_session.flush()
print(tx)

tx.base_balance = 10
tx.quote_balance = 100

print(tx.base_amount)
print(tx.quote_amount)