FROM continuumio/miniconda3
RUN apt-get update && apt-get install -y apt-utils build-essential redis-tools default-libmysqlclient-dev

# Python packages
RUN pip install flask-bcrypt flask-recaptcha mysqlclient

RUN pip install onetimepass pyqrcode rauth pypng mailchimp3 pycryptodome python-dateutil==2.6.1 passlib

RUN conda config --add channels conda-forge
RUN conda config --set channel_priority strict
RUN conda install libiconv flask-login flask-wtf uwsgi

RUN pip install --upgrade pip

COPY requirements.txt /coincube-back/requirements.txt
RUN pip install -r /coincube-back/requirements.txt

ADD . /coincube-back

WORKDIR /coincube-back

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]